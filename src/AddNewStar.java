import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.gson.JsonObject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * A servlet that takes input from a html <form> and talks to MySQL moviedbexample,
 * generates output as a html <table>
 */

// Declaring a WebServlet called FormServlet, which maps to url "/form"
@WebServlet(name = "AddNewStar", urlPatterns = "/api/add-star")
public class AddNewStar extends HttpServlet {

    // Create a dataSource which registered in web.xml
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb-master");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    // Use http Post?
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // Output stream to STDOUT
        PrintWriter out = response.getWriter();
        try {
            // Create a new connection to database
            Connection conn = dataSource.getConnection();

            // Retrieve parameters from the http request.
            String starName = request.getParameter("starname");
            String birthYear = "";
            birthYear = request.getParameter("birthyear");
            if(birthYear.isEmpty()){
                birthYear = "0";
            }
            //Get the last star and generate the next starId
            String nextStarId = generateNewStarID(conn);

            // Generate a SQL query
            String query = "{CALL add_star(?,?,?)}";
            // Declare a new statement
            CallableStatement statement = conn.prepareCall(query);
            statement.setString(1, nextStarId);
            statement.setString(2, starName);
            statement.setInt(3, Integer.parseInt(birthYear));

            JsonObject responseJsonObject = new JsonObject();

            // Log to localhost log
            request.getServletContext().log("query：" + query);
            //perform query
            int rowsInserted = statement.executeUpdate();
            String onSuccess = "Success! starID: " + nextStarId;

            if (rowsInserted > 0){
                responseJsonObject.addProperty("status", "success");
                responseJsonObject.addProperty("message", onSuccess);

            }else{
                // Add fail
                responseJsonObject.addProperty("status", "fail");
                responseJsonObject.addProperty("debug", "0");
                // Log to localhost log
                request.getServletContext().log("Add failed");
                //error message
                responseJsonObject.addProperty("message", "Error! Check the generated starId is correct?!");
                response.getWriter().write(responseJsonObject.toString());
            }

            response.getWriter().write(responseJsonObject.toString());
            statement.close();
//            conn.close();
        } catch (Exception e) {
            // Write error message JSON object to output
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());
        }
        out.close();
    }
    //use this function in addStarServlet
    public String generateNewStarID(Connection dbCon) throws SQLException {
        String query = "{CALL getLastStarId()}";
        CallableStatement statement = dbCon.prepareCall(query);
        ResultSet resultSet = statement.executeQuery();
        String lastStarId = "";
        while(resultSet.next()){
            lastStarId = resultSet.getString("id");
        }
        String sub = lastStarId.substring(2,9);
        int x = Integer.parseInt(sub) + 1;
        String nextStarId = "nm" + x;
        statement.close();
        resultSet.close();
        return nextStarId;
    }

}



