import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/check-session")
public class CheckSessionServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            System.out.println(request.getSession().getAttribute("user"));
            if (request.getSession().getAttribute("user") == null) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // 401 status code
                response.getWriter().write("Session Invalid");
            } else {
                response.setStatus(HttpServletResponse.SC_OK); // 200 status code
                response.getWriter().write("Session Valid");
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
