import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.gson.JsonObject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * A servlet that takes input from a html <form> and talks to MySQL moviedbexample,
 * generates output as a html <table>
 */

// Declaring a WebServlet called FormServlet, which maps to url "/form"
@WebServlet(name = "AddNewMovie", urlPatterns = "/api/add-movie")
public class AddNewMovie extends HttpServlet {

    // Create a dataSource which registered in web.xml
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb-master");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    // Use http Post?
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // Output stream to STDOUT
        PrintWriter out = response.getWriter();
        try {
            // Create a new connection to database
            Connection conn = dataSource.getConnection();

            // Retrieve parameters from the http request.
            String title = request.getParameter("title");
            String year = request.getParameter("year");
            String director = request.getParameter("director");
            String starName = request.getParameter("star"); //fix here, star id is not retrieved from the star name yet
            String birthYear = request.getParameter("birthyear");
            if(birthYear == null || birthYear.isEmpty()){
                birthYear = "null";
            }
            String genreName = request.getParameter("genre");

            //Check if the movie is already in the DB first
            int result = checkMovieInDB(conn, title, year, director);
            if (result == 1){//the movie already exists in db
                JsonObject responseJsonObject = new JsonObject();
                // Login fail
                responseJsonObject.addProperty("status", "fail");
                responseJsonObject.addProperty("debug", "0");
                // Log to localhost log
                request.getServletContext().log("Add failed");
                //error message
                responseJsonObject.addProperty("message", "Success! Error: Duplicated movie!");
                response.getWriter().write(responseJsonObject.toString());
//                conn.close();
                return;
            }

            //Generate the next movieId
            String nextMovieId = generateNewMovieID(conn);

            //Generate a new star id
            AddNewStar newstar = new AddNewStar();
            String nextStarId = newstar.generateNewStarID(conn);

            //Check genre exists in db and add it into db
            int genreId = checkGenreInDB(conn, genreName);
            int nextGenreId = genreId;
            if(genreId == 0){
                //generate new genre id
                nextGenreId = generateNewGenreID(conn);
                //insert the new genre into the genre table
                String query_insert_newGenre = "{CALL add_genre(?,?)}";
                //Declare a new statement
                CallableStatement statement_insert_newgenre = conn.prepareCall(query_insert_newGenre);
                statement_insert_newgenre.setInt(1, nextGenreId);
                statement_insert_newgenre.setString(2, genreName);
                statement_insert_newgenre.executeQuery();
                statement_insert_newgenre.close();
            }

            // Generate a SQL query
            String query = "{CALL add_movie(?,?,?,?)}";
            CallableStatement statement = conn.prepareCall(query);
            statement.setString(1, nextMovieId);
            statement.setString(2, title);
            statement.setInt(3,Integer.parseInt(year));
            statement.setString(4, director);

            JsonObject responseJsonObject = new JsonObject();

            // Log to localhost log
            request.getServletContext().log("query：" + query);
            //perform query
            int rowsInserted = statement.executeUpdate();
            String onSuccess = "Success! movieID: " + nextMovieId + ", starID: " + nextStarId + ", genreID: " + nextGenreId;
            if (rowsInserted > 0){
                responseJsonObject.addProperty("status", "success");
                responseJsonObject.addProperty("message", onSuccess);
            }
            response.getWriter().write(responseJsonObject.toString());


            //insert the new star into the star table
            String query_insert_newstar = "{CALL add_star(?,?,?)}";
            CallableStatement statement_insert_newstar = conn.prepareCall(query_insert_newstar);
            statement_insert_newstar.setString(1, nextStarId);
            statement_insert_newstar.setString(2, starName);

            if(birthYear.equals("null")) {
                statement_insert_newstar.setInt(3, 0);
            }else{
                statement_insert_newstar.setInt(3, Integer.parseInt(birthYear));
            }
            statement_insert_newstar.executeQuery();
            statement_insert_newstar.close();


            //Add a relationship for the new star with the movie id
            String query_movie_star_relationship = "{CALL add_movie_star_relationship(?,?)}";
            CallableStatement statement_movie_star_relationship = conn.prepareCall(query_movie_star_relationship);
            statement_movie_star_relationship.setString(1, nextStarId);
            statement_movie_star_relationship.setString(2, nextMovieId);
            statement_movie_star_relationship.executeQuery();
            statement_movie_star_relationship.close();

            //Add a relationship for the new genre with the movie id
            String query_genre_movie_relationship = "{CALL add_genre_movie_relationship(?,?)}";
            CallableStatement statement_genre_movie_relationship = conn.prepareCall(query_genre_movie_relationship);
            statement_genre_movie_relationship.setInt(1, nextGenreId);
            statement_genre_movie_relationship.setString(2, nextMovieId);
            statement_genre_movie_relationship.executeQuery();
            statement_genre_movie_relationship.close();
//            conn.close();
            statement.close();
        } catch (Exception e) {
            // Write error message JSON object to output
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());
        }
        out.close();
    }
    public int checkMovieInDB(Connection dbCon, String title, String year, String director) throws SQLException {
        int int_year = Integer.parseInt(year);
        String query = "{CALL checkMovieInDB(?, ?, ?)}";
        CallableStatement statement = dbCon.prepareCall(query);
        statement.setString(1, title);
        statement.setInt(2, int_year);
        statement.setString(3, director);
        ResultSet resultSet = statement.executeQuery();
        int result = 0;
        while(resultSet.next()){
            result = resultSet.getInt("result");
        }
        statement.close();
        resultSet.close();
        return result;
    }
    public String generateNewMovieID(Connection dbCon) throws SQLException {
        String query = "{CALL getLastMovieId()}";
        CallableStatement statement = dbCon.prepareCall(query);
        ResultSet resultSet = statement.executeQuery();
        String lastMovieId = "";
        while(resultSet.next()){
            lastMovieId = resultSet.getString("id");
        }
        String sub = lastMovieId.substring(3,9);
        int x = Integer.parseInt(sub) + 1;
        String nextMovieId = "tt0" + x;
        statement.close();
        resultSet.close();
        return nextMovieId;
    }
    public String checkStarInDB(Connection dbCon, String starName) throws SQLException {
        String query = "{CALL checkStarInDB(?)}";
        CallableStatement statement = dbCon.prepareCall(query);
        statement.setString(1, starName);
        ResultSet resultSet = statement.executeQuery();
        String result = "";
        while(resultSet.next()){
            result = resultSet.getString("id");
        }
        statement.close();
        resultSet.close();
        return result;
    }
    public int checkGenreInDB(Connection dbCon, String genreName) throws SQLException {
        String query = "{CALL checkGenreInDB(?)}";
        CallableStatement statement = dbCon.prepareCall(query);
        statement.setString(1, genreName);
        ResultSet resultSet = statement.executeQuery();
        int result = 0;
        while(resultSet.next()){
            result = resultSet.getInt("id");
        }
        statement.close();
        resultSet.close();
        return result;
    }

    public int generateNewGenreID(Connection dbCon) throws SQLException {
        String query = "{CALL getLastGenreId()}";
        CallableStatement statement = dbCon.prepareCall(query);
        ResultSet resultSet = statement.executeQuery();
        int lastStarId = 0;
        while(resultSet.next()){
            lastStarId = resultSet.getInt("id");
            lastStarId = lastStarId + 1;
        }
        statement.close();
        resultSet.close();
        return lastStarId;
    }
}


