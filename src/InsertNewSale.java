import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.gson.JsonObject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * A servlet that takes input from a html <form> and talks to MySQL moviedbexample,
 * generates output as a html <table>
 */

// Declaring a WebServlet called FormServlet, which maps to url "/form"
@WebServlet(name = "InsertNewSale", urlPatterns = "/api/newSale")
public class InsertNewSale extends HttpServlet {

    // Create a dataSource which registered in web.xml
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    // Use http Post?
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // Output stream to STDOUT
        PrintWriter out = response.getWriter();
        try {
            // Create a new connection to database
            Connection conn = dataSource.getConnection();

            String query = "{call INSERT_NEW_SALE (?, ?, ?, ? )}";

            // Declare a new statement
            CallableStatement statement = conn.prepareCall(query);

            String c_id = request.getParameter("c_id");
            String m_id = request.getParameter("m_id");
            String salesDate = request.getParameter("salesDate");
            String qty = request.getParameter("qty");

            statement.setString(1,c_id);
            statement.setString(2,m_id);
            statement.setString(3,salesDate);
            statement.setString(4,qty);

            // Log to localhost log
            request.getServletContext().log("query：" + query);

            // Perform the query
            ResultSet rs = statement.executeQuery();

            JsonObject responseJsonObject = new JsonObject();
            String result = "";

            while(rs.next()) {
                result = rs.getString("result");
            }

            if (result.equals("1")){
                responseJsonObject.addProperty("status", "success");
                responseJsonObject.addProperty("message", "success");

            }else{
                // Login fail
                responseJsonObject.addProperty("status", "fail");
                responseJsonObject.addProperty("debug", result);
                // Log to localhost log
                request.getServletContext().log("Checkout failed");
                //error message
                responseJsonObject.addProperty("message", "card information is incorrect");
            }
            response.getWriter().write(responseJsonObject.toString());
            // Close all structures
            rs.close();
            statement.close();
            conn.close();
            response.setStatus(200);

        } catch (Exception e) {
            request.getServletContext().log("Error: ", e);
            response.setStatus(500);
            // Output Error Message to html
            out.println(String.format("<html><head><title>MovieDBExample: Error</title></head>\n<body><p>SQL error in doGet: %s</p></body></html>", e.getMessage()));
            return;
        }finally {
            out.close();
        }
    }
}


