import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.RequestDispatcher;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet(name = "Top20Movies", urlPatterns = "/api/top20-movies")
public class Top20Movies extends HttpServlet {
    private static final long serialVersionUID = 2L;
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");

        // Output stream to STDOUT
        PrintWriter out = response.getWriter();
        String id = request.getParameter("id");

        try (Connection conn = dataSource.getConnection()) {
            // Construct a query with parameter represented by "?"
            String query1 =  "{CALL GetTop20Movies()}";
            // Declare our statement
            CallableStatement statement1 = conn.prepareCall(query1);
            // Perform the query
            ResultSet rs1 = statement1.executeQuery();
            JsonArray jsonArray = new JsonArray();

            // Iterate through each row of rs
            while (rs1.next()) {
                String queryMovieId = rs1.getString("id");
                String movieTitle = rs1.getString("title");
                String movieYear = rs1.getString("year");
                String movieDirector = rs1.getString("director");
                String movieRating = rs1.getString("rating");

                // Create a JsonObject based on the data we retrieve from rs
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("movie_id", queryMovieId);
                jsonObject.addProperty("movie_name", movieTitle);
                jsonObject.addProperty("movie_year", movieYear);
                jsonObject.addProperty("movie_director", movieDirector);
                jsonObject.addProperty("movie_rating", movieRating);

                // Add second query get the genres from movie id
                String query2 = "{CALL Top20Movies_GetGenresFromMovieId(?)}";
                // Declare our statement
                CallableStatement statement2 = conn.prepareCall(query2);

                statement2.setString(1,queryMovieId);
                // Perform the query
                ResultSet rs2 = statement2.executeQuery();
                StringBuilder tmp = new StringBuilder();
                while (rs2.next()){
                    tmp.append(rs2.getString("name")).append(", ");
                }
                jsonObject.addProperty("movie_genres", tmp.toString().trim().substring(0,tmp.length()-2));

                //Add third query into the list
                String query3 = "{CALL Top20Movies_GetStarsFromMovieId(?)}";
                // Declare our statement
                CallableStatement statement3 = conn.prepareCall(query3);
                statement3.setString(1, queryMovieId);
                // Perform the query

                ResultSet rs3 = statement3.executeQuery();
                JsonArray moviesStars = new JsonArray();
                while (rs3.next()){
                    JsonObject starObj = new JsonObject();
                    String star_id = rs3.getString("id");
                    String star_name = rs3.getString("name");
                    starObj.addProperty("star_id",star_id);
                    starObj.addProperty("star_name",star_name);
                    moviesStars.add(starObj);
                }

                jsonObject.add("movie_stars",moviesStars);
                jsonArray.add(jsonObject);
                rs2.close();
                rs3.close();
                statement2.close();
                statement3.close();
            }
            rs1.close();
            statement1.close();

            // Write JSON string to output
            out.write(jsonArray.toString());
            // Set response status to 200 (OK)
            response.setStatus(200);

        }catch (Exception e) {
            // Write error message JSON object to output
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());

            // Log error to localhost log
            request.getServletContext().log("Error:", e);
            // Set response status to 500 (Internal Server Error)
            response.setStatus(500);
        }finally {
            out.close();
        }
    }

}