import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;


// Declaring a WebServlet called StarsServlet, which maps to url "/api/stars"
@WebServlet(name = "BrowseByGenres", urlPatterns = "/api/browse-by-genre")
public class BrowseByGenres extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // Create a dataSource which registered in web.
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("application/json"); // Response mime type

        // Output stream to STDOUT
        PrintWriter out = response.getWriter();

        String genre = request.getParameter("genre");
        String limit = request.getParameter("limit");
        String page = request.getParameter("page");
        String sort = request.getParameter("sort");

        // Get a connection from dataSource and let resource manager close the connection after usage.
        try (Connection conn = dataSource.getConnection()) {

            String query = "{CALL BrowseGenres(?,?,?,?)}";

            CallableStatement statement = conn.prepareCall(query);

            statement.setString(1,genre);
            statement.setInt(2,Integer.parseInt(limit));
            statement.setInt(3,Integer.parseInt(page));
            statement.setInt(4,Integer.parseInt(sort));

            // Perform the query
            ResultSet rs = statement.executeQuery();

            JsonArray jsonArray = new JsonArray();

            // Iterate through each row of rs
            while (rs.next()) {
                String queryMovieId = rs.getString("movie_id");
                String movieTitle = rs.getString("title");
                String movieYear = rs.getString("year");
                String movieDirector = rs.getString("director");
                String movieRating = rs.getString("rating");

                if (rs.wasNull()){
                    movieRating = "N/A";
                }
                String all_genres = rs.getString("all_genres");
                String[] arr_genres = all_genres.split(",");

                String all_genres_id = rs.getString("all_genres_id");
                String[] arr_genres_id = all_genres_id.split(",");

                String all_stars = rs.getString("all_stars");
                String[] arr_all_stars = all_stars.split(",");

                String all_stars_id = rs.getString("all_stars_id");
                String[] arr_all_stars_id = all_stars_id.split(",");

                // Create a JsonObject based on the data we retrieve from rs
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("movie_id", queryMovieId);
                jsonObject.addProperty("movie_name", movieTitle);
                jsonObject.addProperty("movie_year", movieYear);
                jsonObject.addProperty("movie_director", movieDirector);
                jsonObject.addProperty("movie_rating", movieRating);

                //Create another jsonObject for genre
                JsonArray genreJsonArray = new JsonArray();
                for (int i = 0; i < Math.min(3, arr_genres.length); i++) {
                    JsonObject genreJsonObj = new JsonObject();
                    genreJsonObj.addProperty("genre_id",arr_genres_id[i]);
                    genreJsonObj.addProperty("genre_name",arr_genres[i]);
                    genreJsonArray.add(genreJsonObj);
                }

                jsonObject.add("movie_genres",genreJsonArray);

                //Create another jsonObject for star
                JsonArray starJsonArray = new JsonArray();
                for (int i = 0; i < Math.min(3, arr_all_stars.length); i++) {
                    JsonObject starJsonObj = new JsonObject();
                    starJsonObj.addProperty("star_id",arr_all_stars_id[i]);
                    starJsonObj.addProperty("star_name",arr_all_stars[i]);
                    starJsonArray.add(starJsonObj);
                }

                jsonObject.add("movie_stars",starJsonArray);
                jsonArray.add(jsonObject);
            }
            rs.close();
            statement.close();

            if (page.equals("1")){
                String queryAllPages = "CALL BrowseGenresAllPages(?)";

                CallableStatement stmt = conn.prepareCall(queryAllPages);

                stmt.setString(1,genre);

                ResultSet resultSet = stmt.executeQuery();

                while (resultSet.next()){
                    String allPage = resultSet.getString("total_count");
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("total_pages",Integer.parseInt(allPage));
                    jsonArray.add(jsonObject);
                }
                resultSet.close();
                stmt.close();
            }
            // Log to localhost log
            request.getServletContext().log("getting " + jsonArray.size() + " results");

            // Write JSON string to output
            out.write(jsonArray.toString());
            // Set response status to 200 (OK)
            response.setStatus(200);

        } catch (Exception e) {

            // Write error message JSON object to output
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());

            // Set response status to 500 (Internal Server Error)
            response.setStatus(500);
        } finally {
            out.close();
        }
        // Always remember to close db connection after usage. Here it's done by try-with-resources
    }
}
