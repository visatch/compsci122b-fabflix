import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet(name = "getMoviePriceByID", urlPatterns = "/api/single-movie-price")
public class getMoviePriceByID extends HttpServlet {
    private DataSource dataSource;
    @Override
    public void init(ServletConfig config) throws ServletException {
        try{
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        }catch (NamingException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");

        // Retrieve parameter id from url request.
        String id = req.getParameter("id");

        // The log message can be found in localhost log
        req.getServletContext().log("getting id: " + id);

        // Output stream to STDOUT
        PrintWriter out = resp.getWriter();

        try(Connection conn = dataSource.getConnection()){
            // Construct a query with parameter represented by "?"
            String query = "{CALL GetPriceFromMovieID(?)}";

            // Declare our statement
            CallableStatement statement = conn.prepareCall(query);

            statement.setString(1,id);

            ResultSet rs = statement.executeQuery();

            JsonObject jsonObject = new JsonObject();
            while (rs.next()){
                jsonObject.addProperty("price",rs.getString("price"));
            }
            rs.close();
            statement.close();

            out.write(jsonObject.toString());
            resp.setStatus(200);

        }catch (Exception e){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());
            resp.setStatus(500);
        }finally {
            out.close();
        }
    }
}
