import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Objects;

@WebServlet(name = "BrowseBySearchKeyTerms", urlPatterns = "/api/search-by-keys")
public class BrowseBySearchKeyTerms extends HttpServlet {
    private static int counter = 1;
    private static final long serialVersionUID = 1L;

    // Create a dataSource which registered in web.
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        long startTimeTS = System.nanoTime();

        response.setContentType("application/json"); // Response mime type

        // Output stream to STDOUT
        PrintWriter out = response.getWriter();

        String title = (request.getParameter("title") != null) ? request.getParameter("title") : "null";
        String year = (request.getParameter("year") != null) ? request.getParameter("year") : "null";
        String director = (request.getParameter("director") != null) ? request.getParameter("director")  : "null";
        String star = (request.getParameter("star") != null) ? request.getParameter("star") : "null";
        String page = (request.getParameter("page") != null) ? request.getParameter("page") : "1";
        String limit = (request.getParameter("limit") != null) ? request.getParameter("limit") : "25" ;
        String sort = (request.getParameter("sort") != null) ? request.getParameter("sort") : "0";

        // Get a connection from dataSource and let resource manager close the connection after usage.
        try (out; Connection conn = dataSource.getConnection()) {

            String query = "{CALL SearchMovies(?,?,?,?,?,?,?)}";

            CallableStatement statement = conn.prepareCall(query);

            statement = setParam(statement,title,year,director,star);
            statement.setInt(5,Integer.parseInt(limit));
            statement.setInt(6,Integer.parseInt(page));
            statement.setInt(7,Integer.parseInt(sort));

            //!TJ1 Start
            long startTimeTJ1 = System.nanoTime();
            // Perform the query
            ResultSet rs = statement.executeQuery();

            JsonArray jsonArray = new JsonArray();

            // Iterate through each row of rs
            while (rs.next()) {
                String queryMovieId = rs.getString("movie_id");
                String movieTitle = rs.getString("title");
                String movieYear = rs.getString("year");
                String movieDirector = rs.getString("director");
                String movieRating = rs.getString("rating");

                if (rs.wasNull()){
                    movieRating = "N/A";
                }
                String all_genres = rs.getString("all_genres");
                String[] arr_genres = all_genres.split(",");

                String all_genres_id = rs.getString("all_genres_id");
                String[] arr_genres_id = all_genres_id.split(",");

                String all_stars = rs.getString("all_stars");
                String[] arr_all_stars = all_stars.split(",");

                String all_stars_id = rs.getString("all_stars_id");
                String[] arr_all_stars_id = all_stars_id.split(",");

                // Create a JsonObject based on the data we retrieve from rs
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("movie_id", queryMovieId);
                jsonObject.addProperty("movie_name", movieTitle);
                jsonObject.addProperty("movie_year", movieYear);
                jsonObject.addProperty("movie_director", movieDirector);
                jsonObject.addProperty("movie_rating", movieRating);

                //Create another jsonObject for genre
                JsonArray genreJsonArray = new JsonArray();
                for (int i = 0; i < Math.min(3, arr_genres.length); i++) {
                    JsonObject genreJsonObj = new JsonObject();
                    genreJsonObj.addProperty("genre_id",arr_genres_id[i]);
                    genreJsonObj.addProperty("genre_name",arr_genres[i]);
                    genreJsonArray.add(genreJsonObj);
                }
                jsonObject.add("movie_genres",genreJsonArray);

                //Create another jsonObject for star
                JsonArray starJsonArray = new JsonArray();
                for (int i = 0; i < Math.min(3, arr_all_stars.length); i++) {
                    JsonObject starJsonObj = new JsonObject();
                    starJsonObj.addProperty("star_id",arr_all_stars_id[i]);
                    starJsonObj.addProperty("star_name",arr_all_stars[i]);
                    starJsonArray.add(starJsonObj);
                }

                jsonObject.add("movie_stars",starJsonArray);
                jsonArray.add(jsonObject);
            }

            //! Tj1 End
            long endTimeTJ1 = System.nanoTime();

            //! TJ1 ElapseTime
            long elapsedTimeTJ1 = endTimeTJ1 - startTimeTJ1;

            rs.close();
            statement.close();

            //Another the query for total page, do this for all pages.
            String queryAllPages = "{CALL SearchMovies_CountRecords(?,?,?,?)}";

            CallableStatement stmt = conn.prepareCall(queryAllPages);

            stmt = setParam(stmt,title,year,director,star);

            //! TJ2
            long startTimeTJ2 = System.nanoTime();

            ResultSet resultSet = stmt.executeQuery();


            while (resultSet.next()){
                String allPage = resultSet.getString("total_count");
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("total_pages",Integer.parseInt(allPage));
                jsonArray.add(jsonObject);
            }

            //! TJ2 End
            long endTimeTJ2 = System.nanoTime();

            //! TJ2 ElapseTime
            long elapsedTimeTJ2 = endTimeTJ2 - startTimeTJ2;

            resultSet.close();
            stmt.close();

            // Log to localhost log
            request.getServletContext().log("getting " + jsonArray.size() + " results");

            // Write JSON string to output
            out.write(jsonArray.toString());
            // Set response status to 200 (OK)
            response.setStatus(200);

            long endTimeTS = System.nanoTime();

            //? TS
            long elapsedTimeTS = endTimeTS - startTimeTS; // elapsed time in nano seconds. Note: print the values in nanoseconds
            request.getServletContext().log("TS: " + Long.toString(elapsedTimeTS));

            //? TJ
            long elapsedTimeTJ = elapsedTimeTJ1 + elapsedTimeTJ2;
            request.getServletContext().log("TJ: " + Long.toString(elapsedTimeTJ));

            try{
                //String path = "/Users/visa/Downloads/";
                String path = "/var/lib/tomcat10/logs";
//                String path = getServletContext().getRealPath("/");

                String file_pathTS = "result-TS-" + counter + ".txt";
                File fileTS = new File(path,file_pathTS);
                fileTS.createNewFile();

                FileWriter fwTS = new FileWriter(fileTS,true);
                fwTS.append(Long.toString(elapsedTimeTS)).append('\n');
                fwTS.close();

                String file_pathTJ = "result-TJ-" + counter + ".txt";
                File fileTJ = new File(path,file_pathTJ);
                fileTJ.createNewFile();
                FileWriter fwTJ = new FileWriter(fileTJ,true);
                fwTJ.append(Long.toString(elapsedTimeTJ)).append('\n');
                fwTJ.close();

            }catch (IOException e){
                e.printStackTrace();
            }

        } catch (Exception e) {

            // Write error message JSON object to output
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());
            // Set response status to 500 (Internal Server Error)
            response.setStatus(500);
        } finally {
            out.close();
        }
    }

    CallableStatement setParam(CallableStatement stmt, String title, String year, String director, String star) throws SQLException{
        // Title
        if (title.equals("null") || title.isEmpty())
            stmt.setNull(1, Types.VARCHAR);
        else
            stmt.setString(1,title);

        // Year

        if (year.equals("null") || year.isEmpty()){
            stmt.setNull(2,Types.INTEGER);
        }else{
            stmt.setInt(2,Integer.parseInt(year));
        }

        if (director.equals("null") || director.isEmpty())
            stmt.setNull(3,Types.VARCHAR);
        else
            stmt.setString(3,director);

        if (star.equals("null") || star.isEmpty())
            stmt.setNull(4,Types.VARCHAR);
        else
            stmt.setString(4,star);

        return stmt;
    }
}
