import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet(name = "SingleMovieServlet", urlPatterns = "/api/single-movie")
public class SingleMovieServlet extends HttpServlet {
    private DataSource dataSource;
    @Override
    public void init(ServletConfig config) throws ServletException {
        try{
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        }catch (NamingException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");

        // Retrieve parameter id from url request.
        String id = req.getParameter("id");

        // The log message can be found in localhost log
        req.getServletContext().log("getting id: " + id);

        // Output stream to STDOUT
        PrintWriter out = resp.getWriter();

        try(Connection conn = dataSource.getConnection()){
            // Construct a query with parameter represented by "?"
            String query = "{CALL SingleMovie(?)}";

            // Declare our statement
            CallableStatement statement = conn.prepareCall(query);

            statement.setString(1,id);

            ResultSet rs = statement.executeQuery();

            JsonArray jsonArray = new JsonArray();
            boolean flag = false;
            JsonObject jsonObject = new JsonObject();
            //create an star array for each movie
            JsonArray moviesStars = new JsonArray();

            while (rs.next()){
                JsonObject starObj = new JsonObject();
                if(!flag) {
                    String movieTitle = rs.getString("title");
                    String movieYear = rs.getString("year");
                    String movieDirector = rs.getString("director");
                    String movieRating = rs.getString("rating");
                    String movieGenres = rs.getString("all_genres");
                    String starId = rs.getString("s.id");
                    String starName = rs.getString("name");

                    jsonObject.addProperty("movie_title", movieTitle);
                    jsonObject.addProperty("movie_year", movieYear);
                    jsonObject.addProperty("movie_director", movieDirector);
                    jsonObject.addProperty("movie_rating", movieRating);
                    jsonObject.addProperty("movie_genres", movieGenres);
                    starObj.addProperty("star_id", starId);
                    starObj.addProperty("star_name", starName);
                    moviesStars.add(starObj);
                    flag = true;
                    continue;
                }
                String starId = rs.getString("s.id");
                String starName = rs.getString("name");
                starObj.addProperty("star_id", starId);
                starObj.addProperty("star_name", starName);
                moviesStars.add(starObj);
            }
            jsonObject.add("movie_stars",moviesStars);
            jsonArray.add(jsonObject);
            rs.close();
            statement.close();

            out.write(jsonArray.toString());
            resp.setStatus(200);

        }catch (Exception e){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorMessage", e.getMessage());
            out.write(jsonObject.toString());
            resp.setStatus(500);
        }finally {
            out.close();
        }
    }
}
