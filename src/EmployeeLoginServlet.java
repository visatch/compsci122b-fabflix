import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.gson.JsonObject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jasypt.util.password.StrongPasswordEncryptor;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * A servlet that takes input from a html <form> and talks to MySQL moviedbexample,
 * generates output as a html <table>
 */

// Declaring a WebServlet called FormServlet, which maps to url "/form"
@WebServlet(name = "EmployeeLoginServlet", urlPatterns = "/api/employee-login")
public class EmployeeLoginServlet extends HttpServlet {

    // Create a dataSource which registered in web.xml
    private DataSource dataSource;

    public void init(ServletConfig config) {
        try {
            dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/moviedb");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    // Use http Post?
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // Output stream to STDOUT
        PrintWriter out = response.getWriter();
        try {
            JsonObject responseJsonObject = new JsonObject();

            String gRecaptchaResponse = request.getParameter("g-recaptcha-response");

            try {
                RecaptchaVerifyUtils.verify(gRecaptchaResponse);
            } catch (Exception e) {
                responseJsonObject.addProperty("message", e.getMessage());
                response.getWriter().write(responseJsonObject.toString());
                out.close();
                return;
            }

            // Create a new connection to database
            Connection conn = dataSource.getConnection();

            // Retrieve parameter "name" from the http request, which refers to the value of <input name="name"> in homepage.html
            String inputEmail = request.getParameter("employee_email");
            String inputPassword = request.getParameter("employee_password");

            // Generate a SQL query
            String query = "{CALL GetEmployeePwdByEmail(?)}";

            // Declare a new statement
            CallableStatement statement = conn.prepareCall(query);

            statement.setString(1, inputEmail);

            ResultSet rs = statement.executeQuery();

            boolean success = false;
            if (rs.next()) {
                String encryptedPassword = rs.getString("password");

                success = new StrongPasswordEncryptor().checkPassword(inputPassword, encryptedPassword);

                if (success){
                    request.getSession().setAttribute("admin", new Admin(inputEmail));
                    Cookie cookie = new Cookie("admin", inputEmail);
                    cookie.setPath("/");
                    cookie.setMaxAge(60*60*2); // 2 hours
                    response.addCookie(cookie);
                    responseJsonObject.addProperty("status", "success");
                    responseJsonObject.addProperty("message", "success");
                }
            }

            if (!success){
                // Login fail
                responseJsonObject.addProperty("status", "fail");
                responseJsonObject.addProperty("email-input", inputEmail);
                responseJsonObject.addProperty("password-input", inputPassword);
                // Log to localhost log
                request.getServletContext().log("Login failed");
                //error message
                responseJsonObject.addProperty("message", "email or password is incorrect");
            }

            response.getWriter().write(responseJsonObject.toString());

            // Close all structures
            rs.close();
            statement.close();
//            conn.close();

        } catch (Exception e) {
            /*
             * After you deploy the WAR file through tomcat manager webpage,
             *   there's no console to see the print messages.
             * Tomcat append all the print messages to the file: tomcat_directory/logs/catalina.out
             *
             * To view the last n lines (for example, 100 lines) of messages you can use:
             *   tail -100 catalina.out
             * This can help you debug your program after deploying it on AWS.
             */
            request.getServletContext().log("Error: ", e);

            // Output Error Message to html
            out.println(String.format("<html><head><title>MovieDBExample: Error</title></head>\n<body><p>SQL error in doGet: %s</p></body></html>", e.getMessage()));
            return;
        }
        out.close();
    }
}


