## <p align="center"> CS 122B </p>

# Authors: [Toan] [Visa] 

## Website Hosting:
    - [visatch.dev (Scaled Version)](https://visatch.dev)
    - [visatch.dev:8443 (Single Instance)](https://visatch.dev:8443)


## Demo Video
- View our Project 1 Demo Video here: [Google Drive](https://drive.google.com/file/d/1PoUsaEQsMQqeeQl4-Ig1q3yLtFtdjlS4/view?usp=sharing) [YOUTUBE](https://youtu.be/-a4GYUk2m88)
- View our Project 2 Demo Video here: [Google Drive](https://drive.google.com/file/d/1kGdFJh16wgvKyz4asyvoCSqBgBxIXJUF/view?usp=drive_link) [YOUTUBE](https://youtu.be/xfnrrsmDRas)
- View our Project 3 Demo Video here: [Google Drive](https://drive.google.com/file/d/1DYEEhLE8JUDslsbbclu26HI30-cTFfo2/view?usp=sharing) [YOUTUBE](https://youtu.be/CzT6ijmMp4c)
  [Project 3: Store Procedure File](stored-procedure.sql)
- View our Project 4 Demo Video here: [Google Drive](https://drive.google.com/file/d/1QJDC9cQIXUceMiE-sj28olQB1KZi0_nG/view?usp=drive_link) [YOUTUBE](https://youtu.be/3hkbo5Swbv4)
- View our Project 5 Demo Video here: [Google Drive](https://drive.google.com/file/d/1-G1IsComgEyA4QF2oQJ_BIMlfDwaHjUe/view?usp=drive_link) [YOUTUBE](https://youtu.be/hliMabr3I24)

- # General
  - #### Team#: Team Chocolate

  - #### Names: Toan Van Vo, Visa Touch

  - #### Project 5 Video Demo Link:

  - #### Instruction of deployment:
  - After creating two new instances Master and Slave on AWS:
    - In Master:
      - Access to the server: ssh ubuntu@3.134.116.198 -i ~/ubuntu_key.pem
      - Install and start Tomcat server, install maven, install mysql-server
      - Set up database: mysql -u mytestuser -pteamchocolate moviedb < moviedb.sql
      - Git clone our project: git clone https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate.git
      - Go into the project: cd 2023-fall-cs122b-team_chocolate/
      - Run the project: mvn package
      - Check the currently deployed apps: ls -lah /var/lib/tomcat10/webapps/
      - Copy the war file to tomcat: 'sudo cp ./target/*.war /var/lib/tomcat10/webapps/'
      - Check the currently deployed apps again: ls -lah /var/lib/tomcat10/webapps/
      - Restart Tomcat: sudo service tomcat10 restart
    - In Slave:
      - Access to the server: ssh ubuntu@3.142.222.251 -i ~/ubuntu_key.pem
      - Install and start Tomcat server, install maven, install mysql-server.
      - Set up database: mysql -u mytestuser -pteamchocolate moviedb < moviedb.sql
      - Git clone our project: git clone https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate.git
      - Go into the project: cd 2023-fall-cs122b-team_chocolate/
      - Run the project: mvn package
      - Check the currently deployed apps: ls -lah /var/lib/tomcat10/webapps/
      - Copy the war file to tomcat: 'sudo cp ./target/*.war /var/lib/tomcat10/webapps/'
      - Check the currently deployed apps again: ls -lah /var/lib/tomcat10/webapps/
      - Restart Tomcat: sudo service tomcat10 restart

  - #### Collaborations and Work Distribution: Toan (Task 2 & 3), Visa (Task 1 & 4)
    - Toan Van Vo
      - Setup two new AWS instances
      - Setup inbound rules in the security groups for each instance
      - Install Mysql server for each instance
      - Change configuration /etc/mysql/mysql.conf.d/mysqld.cnf file for each instance
      - Setup database replication between Master and Slave
      - Setup Tomcat on each Master/Slave instance
      - Deploy the project for each instance
      - Setup Apache and its proxy on instance 1 (the original Fabflix instance)
      - Configure the Apache2 webserver to use its proxy_balancer module for sharing requests to the backend instances
        - Configure the proxy on instance 1 to handle sessions properly
      - Make the write requests send to the master MySQL instance only, while the read requests are sent to either the master or the slave MySQL instance
    - Visa Touch (vtouch)
      - Implement Connection Pooling on FabFlix
      - Prepare Proxy server (instance1), Primary, Secondary servers and codebase to execute test plans
      - Prepare test plans on Jmeter
      - Setting up the log time for TS & TJ on Fabflix (Search function)
      - Create log_processing.py 
      - Collecting the performance results
      - Write the report
      
- # Connection Pooling
  - #### Include the filename/path of all code/configuration files in GitHub of using JDBC Connection Pooling.
    - Connection Pool is used in all codes in the project. One of which is [Search Function](src/BrowseBySearchKeyTerms.java)

  - #### Explain how Connection Pooling is utilized in the Fabflix code.
    - Fabflix is using Connection Pooling by creating a 100 of connections when the application is initialized. So, it is cut down the time to reopen the connection after each use. In the test plans below, it shows a huge improvement in the response time.

  - #### Explain how Connection Pooling works with two backend SQL.
    - The Connection Pooling between two backend SQL is that creates a 100 of connections on each backend SQL, therefore it does not slow down the response time when the response the result.

- # Master/Slave
  - #### Include the filename/path of all code/configuration files in GitHub of routing queries to Master/Slave SQL.
    - https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate/blob/toan/WebContent/META-INF/context.xml
    - https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate/blob/toan/src/AddNewMovie.java
    - https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate/blob/toan/src/AddNewStar.java

  - #### How read/write requests were routed to Master/Slave SQL?
    - The requests are routed to one of the two instances Master/Slave.
    - For read requests, the Master instance will read from its localhost Master SQL, the Slave instance will read from its localhost Slave SQL.
    - For the write requests, only the Master SQL is used.
    - Configuration:
      + In Master instance:
        In the context.xml file, beside the original database resource moviedb, we add one additional resource which use the same localhost database but with a different name (moviedb-master).
        The datasource connection in the writing API (Add movie servlet, add star servlet) is changed to this new database resource moviedb-master.
      + In Slave instance:
        In the context.xml file, beside the original database resource moviedb, we add one additional resource which use database resource of the master instance moviedb-master.
        The datasource connection in the writing API (Add movie servlet, add star servlet) is changed to this new database resource moviedb-master.


- # JMeter TS/TJ Time Logs
  - #### Instructions of how to use the [`log_processing.py`](log_processing.py) script to process the JMeter logs.
    - The script is written in python using two arguments, first_file is TS file and second_file is TJ file. 
    - For example, python3 process_query_time.py logs/1-http-10-threads-no-connection-pool-TS.txt logs/1-http-10-threads-no-connection-pool-TJ.txt


- # JMeter TS/TJ Time Measurement Report

| **Single-instance Version Test Plan**         | **Graph Results Screenshot**                                 | **Average Query Time(ms)** | **Average Search Servlet Time(ms)** | **Average JDBC Time(ms)** | **Analysis**                                                                                                                                                                                                                                            |
|-----------------------------------------------|--------------------------------------------------------------|----------------------------|-------------------------------------|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Case 1: HTTP/10 threads/No connection pooling | ![Image](logs/imgs/1-http-10-threads-no-connection-pool.png) | 2508                       | 2400                                | 1751                      | Running 10 threads concurrently causes all connectivity slower than I initially expected. This can be used a baseline to compare with the next test case, how a different setup improve speed in all averages.                                          |
| Case 2: HTTP/1 thread                         | ![Image](logs/imgs/2-http-1-thread.png)                      | 115                        | 34.5685                             | 26.6079                   | Running 1 thread proved to be quick because it is a sequential operations with minimal contention for resources. The query has exclusive access to the CPU, memory, and other system resources it needs to execute, leading to a faster response time.  |
| Case 3: HTTP/10 threads                       | ![Image](logs/imgs/3-http-10-threads.png)                    | 343                        | 239.5336                            | 213.9720                  | Running 10 threads is basically introduce concurrency, which means multiple queries are being processed at the same time, leading to several issues such as resource limited, database bottleneck. As a result, the execution has a long response time. |
| Case 4: HTTPS/10 threads                      | ![Image](logs/imgs/4-https-10-threads.png)                   | 334                        | 234.9571                            | 210.1086                  | Compare this case to previous case does not has a much of a difference since the only difference setup is the protocol of the access point. The difference in response time is minimal, which can be neglected.                                         |

| **Scaled Version Test Plan**                  | **Graph Results Screenshot**                                                | **Average Query Time(ms)** | **Average Search Servlet Time(ms)** | **Average JDBC Time(ms)** | **Analysis**                                                                                                                                                                                       |
|-----------------------------------------------|-----------------------------------------------------------------------------|----------------------------|-------------------------------------|---------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Case 1: HTTP/10 threads/No connection pooling | ![Image](logs/imgs/5-scaled-version-http-no-connection-pool-10-threads.png) | 202                        | 118.0168                            | 107.7033                  | In this scaled version of deployment, it can be seen a huge improvement in term of the response time. However, it is notably that we have 3 servers in total (proxy,primary and secondary server). |
| Case 2: HTTP/1 thread                         | ![Image](logs/imgs/6-scaled-version-http-1-thread.png)                      | 285                        | 31.9464                             | 26.1388                   | The average search servlet and average jdbc time proved that the connection pool cut down the response time by 3 times compared to the previous test plan.                                         |
| Case 3: HTTP/10 threads                       | ![Image](logs/imgs/7-scaled-version-http-10-thread.png)                     | 318                        | 65.1974                             | 57.7664                   | By comparing connection pool and non connection pool, this test plan shows clearly that connection pool only takes half response time compared to the non connection pool.                         |




## Phase 3
## Importing large XML data files into the Fabflix database
- Importing movies from mains243.xml (Using Dom Parser)
+ Two optimization that I utilized to fasten the parser and insert into the database are:
  + Utilize the Java Array List by adding all the data from XML file to it and retrieve it back when adding to the database, which I only need to 
  open connection once. 
  + Using Store Procedure on the Database to speed up the adding data to the database without having to repeatedly send the query from
  java to the database, which takes more time and data.
- **As a result, the entire process of adding more than 8,000 movies only takes less than 8 minutes on AWS.**

_ Importing stars, movies from actors63.xml, casts124.xml (run xmlParser.src.main.SAXParserStar.java file): 

+ When the data is bad such as the dob field is empty, errored or n.a, we will insert the star with empty birth year ('<null>');
If the star name contains a "'" which will cause mysql syntax error, we will use "" to wrap around it; If the movie has a relationship with a star
but the star does not exist in the star table, we will ignore this relationship. 

+ For optimization strategy: 
1. After parsing all the stars information from actors63.xml into a list of star objects, instead of loop through each star, manipulate the fields, 
create connection and make call to the database each time, we will write all the mysql insert statements for all star objects into a file "stars.sql" first,
then we will execute this file, make the database connection once and insert stars into the database. 
2. After parsing all the stars and movie relationships from casts124.xml, we have the star name and movie id. In order to add the relationships 
between the star and movie, we need star id. Instead of calling the database to get the star id when looping through each star, we created 
a global data structure Hashmap to store (star name, star id) when we successfully inserted the star into the database in the previous step. 
Then, we check the star name in the Hashmap to get the star id. 

## Phase 2
## Substring Matching
 - When the users input keywords, we will find all the possible strings that contain the pattern 'keywords' anywhere in the moviedb. The
substrings can start and end with any character but they must contain the keywords in them, and they are case-insensitive. 
 - The syntax we use is "WHERE movie.title LIKE 'CONCAT('%', keywords, '%')" in the queries' condition.


## Project 5
## Instruction of deployment:

After creating two new instances Master and Slave:

In Master: 
- Access to the server: ssh ubuntu@3.134.116.198 -i ~/ubuntu_key.pem
- Install and start Tomcat server, install maven, install mysql-server
- Set up database: mysql -u mytestuser -pteamchocolate moviedb < moviedb.sql
- Git clone our project: git clone https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate.git
- Go into the project: cd 2023-fall-cs122b-team_chocolate/
- Git log
- Run the project: mvn package
- Check the currently deployed apps: ls -lah /var/lib/tomcat10/webapps/
- Copy the war file to tomcat: 'sudo cp ./target/*.war /var/lib/tomcat10/webapps/'
- Check the currently deployed apps again: ls -lah /var/lib/tomcat10/webapps/
- Restart Tomcat: sudo service tomcat10 restart

In Slave:
- Access to the server: ssh ubuntu@3.142.222.251 -i ~/ubuntu_key.pem
- Install and start Tomcat server, install maven, install mysql-server.
- Set up database: mysql -u mytestuser -pteamchocolate moviedb < moviedb.sql
- Git clone our project: git clone https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate.git
- Go into the project: cd 2023-fall-cs122b-team_chocolate/
- Git log
- Run the project: mvn package
- Check the currently deployed apps: ls -lah /var/lib/tomcat10/webapps/
- Copy the war file to tomcat: 'sudo cp ./target/*.war /var/lib/tomcat10/webapps/'
- Check the currently deployed apps again: ls -lah /var/lib/tomcat10/webapps/
- Restart Tomcat: sudo service tomcat10 restart

## Collaborations and Work Distribution:

- Toan:
  + Setup two new AWS instances
  + Setup inbound rules in the security groups for each instance
  + Install Mysql server for each instance
  + Change configuration /etc/mysql/mysql.conf.d/mysqld.cnf file for each instance
  + Setup database replication between Master and Slave
  + Setup Tomcat on each Master/Slave instance
  + Deploy the project for each instance
  + Setup Apache and its proxy on instance 1 (the original Fabflix instance)
  + Configure the Apache2 webserver to use its proxy_balancer module for sharing requests to the backend instances
  + Configure the proxy on instance 1 to handle sessions properly
  + Make the write requests send to the master MySQL instance only, while the read requests are sent to either the master or the slave MySQL instance
  
## Master/Slave
## Include the filename/path of all code/configuration files in GitHub of routing queries to Master/Slave SQL.
  - https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate/blob/toan/WebContent/META-INF/context.xml 
  - https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate/blob/toan/src/AddNewMovie.java
  - https://github.com/uci-jherold2-fall23-cs122b/2023-fall-cs122b-team_chocolate/blob/toan/src/AddNewStar.java

## How read/write requests were routed to Master/Slave SQL?
- The requests are routed to one of the two instances Master/Slave. 
- For read requests, the Master instance will read from its localhost Master SQL, the Slave instance will read from its localhost Slave SQL. 
- For the write requests, only the Master SQL is used. 
- Configuration:
  + In Master instance:
    In the context.xml file, beside the original database resource moviedb, we add one additional resource which use the same localhost database but with a different name (moviedb-master).
    The datasource connection in the writing API (Add movie servlet, add star servlet) is changed to this new database resource moviedb-master.
  + In Slave instance: 
    In the context.xml file, beside the original database resource moviedb, we add one additional resource which use database resource of the master instance moviedb-master.
    The datasource connection in the writing API (Add movie servlet, add star servlet) is changed to this new database resource moviedb-master.