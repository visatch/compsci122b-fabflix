import typing
import argparse


def getAverageFromFile(file: str):
    with open(file,'r') as file:
        count = 0
        total_duration = 0
        for i in file.readlines():
            total_duration += int(i)
            count += 1
        return (total_duration / count) / 1000 / 1000


def main():
    parser = argparse.ArgumentParser(description="Processing time average time")
    parser.add_argument('file1', type=str, help='Path to file TS')
    parser.add_argument('file2',type=str, help='Path to file TJ')
    args = parser.parse_args()

    print("Average TS: " + "%.4f" % getAverageFromFile(args.file1) + " ms")
    print("Average TJ: " + "%.4f" % getAverageFromFile(args.file2) + " ms")


if __name__ == '__main__':
    main()