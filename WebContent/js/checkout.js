

$(document).ready(function(){
    $('#datepicker').datepicker({
        format: "yyyy-mm-dd"
    });

    $('#checkout').click(function (event) {
        event.preventDefault();

        var formData = {
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            card: $('#cardNumber').val(),
            expiration: $('#datepicker').val()
        };

        if (formData.firstName != null && formData.lastName != null && formData.card != null && formData.expiration != null){
            console.log(formData);

            $.ajax({
                type: 'POST',
                url: 'api/checkout',
                data: formData,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    if (response["status"] === "success"){
                        console.log("Payment Confirmed!");
                        alert("Success")
                        $('#error-msg').addClass('hide')
                        window.location.href = 'confirmation.html'
                    }else{
                        console.log("Payment Failed");
                        $('#form-checkout')[0].reset()
                        $('#error-msg').removeClass('hide')
                    }
                },
                error: function () {
                    alert("Payment processor is failing!!!");
                    $('#error-msg').removeClass('hide')
                },
            });
        }
    });


    /**
     * Handle the data returned by LoginServlet
     * @param resultDataString jsonObject
     */
    function handleCheckoutResult(resultDataString) {
        let resultDataJson = JSON.parse(resultDataString);

        console.log("handle checkout response");
        console.log(resultDataJson);
        console.log(resultDataJson["status"]);

        // If login succeeds, it wil' insert the information into database
        if (resultDataJson["status"] === "success") {
            // window.location.replace("index.html");
            //insert information into database table sales (id, customerId, movieId, saleDate)

        } else {
            // If login fails, the web page will display
            // error messages on <div> with id "login_error_message"
            console.log("show error message");
            console.log(resultDataJson["message"]);
            $("#checkout_error_message").text(resultDataJson["message"]);
        }
    }

    /**
     * Submit the form content with POST method
     * @param formSubmitEvent
     */
    // function submitCheckoutForm(formSubmitEvent) {
    //     console.log("submit checkout form");
    //     /**
    //      * When users click the submit button, the browser will not direct
    //      * users to the url defined in HTML form. Instead, it will call this
    //      * event handler when the event is triggered.
    //      */
    //     formSubmitEvent.preventDefault();
    //
    //     $.ajax(
    //         "api/login", {
    //             method: "POST",
    //             // Serialize the login form to the data sent by POST request
    //             data: checkout_form.serialize(),
    //             success: handleCheckoutResult
    //         }
    //     );
    // }

    //Bind the submit action of the form to a handler function
    //checkout_form.submit(submitCheckoutForm);





});

