
let add_movie_form = $("#add_movie_form");

/**
 * Handle the data returned by LoginServlet
 * @param resultDataString jsonObject
 */
function handleAddMovieResult(resultDataString) {
    let resultDataJson = JSON.parse(resultDataString);

    let message = "";

    // If login succeeds, it will redirect the user to top20movies.html
    if (resultDataJson["status"] === "success") {
        // $("#add_movie_message").text(resultDataJson["message"]);
        message = `<p class="text-success">${resultDataJson["message"]}</p> `
    } else {
        // If login fails, the web page will display
        message = `<p class="text-warning">${resultDataJson["message"]}</p> `
    }

    $("#msg").append(message);
}

function submitAddMovieForm(formSubmitEvent) {
    console.log("submit add movie form");
    /**
     * When users click the submit button, the browser will not direct
     * users to the url defined in HTML form. Instead, it will call this
     * event handler when the event is triggered.
     */
    formSubmitEvent.preventDefault();
    $.ajax(
        "/api/add-movie", {
            method: "POST",
            // Serialize the login form to the data sent by POST request
            data: add_movie_form.serialize(),
            success: handleAddMovieResult
        }
    );
}
// Bind the submit action of the form to a handler function
add_movie_form.submit(submitAddMovieForm);