import {removeAllSearchKeyFromLocalStorage} from "./utility.js";


let login_form = $("#login_form");
// form-recaptcha
/**
 * Handle the data returned by LoginServlet
 * @param resultDataString jsonObject
 */
function handleLoginResult(resultDataString) {
    let resultDataJson = JSON.parse(resultDataString);

    // If login succeeds, it will redirect the user to top20movies.html
    if (resultDataJson["status"] === "success") {
        let email = $('#emailAddress').val();
        localStorage.setItem('email',email);
        window.location.replace("index.html");
    } else {
        // If login fails, the web page will display
        // error messages on <div> with id "login_error_message"
        grecaptcha.reset();
        $("#login_error_message").text(resultDataJson["message"]);
    }
}

/**
 * Submit the form content with POST method
 * @param formSubmitEvent
 */
function submitLoginForm(formSubmitEvent) {
    /**
     * When users click the submit button, the browser will not direct
     * users to the url defined in HTML form. Instead, it will call this
     * event handler when the event is triggered.
     */
    formSubmitEvent.preventDefault();

    $.ajax({
        url: 'api/login',
        method: 'POST',
        data: login_form.serialize(),
        success: (data) => {
            console.log(data)
            handleLoginResult(data)
        },
        error: function () {
            console.log("ERROR IN LOGIN")
        },
    });
}

// Bind the submit action of the form to a handler function
login_form.submit(submitLoginForm);

$(document).ready(function () {
    removeAllSearchKeyFromLocalStorage();
});