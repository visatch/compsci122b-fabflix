// import {getCookie} from "./utility.js";
// $(document).ready(function () {
//     if (getCookie("admin") != null) {
//         document.location.replace("admin.html");
//    / }
// });

let employee_login_form = $("#employee_login_form");

/**
 * Handle the data returned by LoginServlet
 * @param resultDataString jsonObject
 */
function handleLoginResult(resultDataString) {
    let resultDataJson = JSON.parse(resultDataString);

    // If login succeeds, it will redirect the user to admin.html
    if (resultDataJson["status"] === "success") {
        let email = $('#emailAddress').val();
        localStorage.setItem('email',email);
        window.location.replace("admin.html");
    } else {
        // If login fails, the web page will display
        // error messages on <div> with id "login_error_message"
        grecaptcha.reset();
        $("#login_error_message").text(resultDataJson["message"]);
    }
}

/**
 * Submit the form content with POST method
 * @param formSubmitEvent
 */
function submitLoginForm(formSubmitEvent) {
    console.log("submit employee login form");
    /**
     * When users click the submit button, the browser will not direct
     * users to the url defined in HTML form. Instead, it will call this
     * event handler when the event is triggered.
     */
    formSubmitEvent.preventDefault();

    $.ajax({
        url: '/api/employee-login',
        method: 'POST',
        data: employee_login_form.serialize(),
        success: (data) => {
            handleLoginResult(data)
        },
        error: function () {
            console.log("ERROR IN LOGIN")
        },
    });
}

// Bind the submit action of the form to a handler function
employee_login_form.submit(submitLoginForm);

