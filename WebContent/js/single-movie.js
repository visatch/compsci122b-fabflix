/**
 * Retrieve parameter from request URL, matching by parameter name
 * @param target String
 * @returns {*}
 */
function getParameterByName(target) {
    // Get request URL
    let url = window.location.href;
    // Encode target parameter name to url encoding
    target = target.replace(/[\[\]]/g, "\\$&");

    // Ues regular expression to find matched parameter value
    let regex = new RegExp("[?&]" + target + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';

    // Return the decoded parameter value
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function handleResult(resultData) {
    // Populate the single movie table
    // Find the empty table body by id "movie_table_body"
    let movieTableBodyElement = jQuery("#single_movie_tbody");

    // Concatenate the html tags with resultData jsonObject to create table rows
    let rowHTML = "";
    rowHTML += "<tr>";
    rowHTML += "<th>" + resultData[0]["movie_title"] + "</th>";
    rowHTML += "<th>" + resultData[0]["movie_year"] + "</th>";
    rowHTML += "<th>" + resultData[0]["movie_director"] + "</th>";
    rowHTML += "<th>" + resultData[0]["movie_genres"] + "</th>";
    rowHTML += "<th>"
    for (let j = 0; j < resultData[0]["movie_stars"].length; j++) {
        let star_name = resultData[0]["movie_stars"][j]["star_name"]
        let star_id = resultData[0]["movie_stars"][j]["star_id"]
        // Add a link to single-star.html with id passed with GET url parameter
        rowHTML += '<a href=single-star.html?id=' + star_id + ' class="text-success" >'
            + star_name +     // display star_name for the link text
            '</a>'

        if (j < resultData[0]["movie_stars"].length - 1)
            rowHTML += ", "
    }
    rowHTML += "</th>";
    rowHTML += "<th>" + resultData[0]["movie_rating"] +
        "<i class=\"fa-regular fa-star fa-beat-fade ms-2\" style=\"color: #118be8;\"></i>" + "</th>";
    rowHTML += "</tr>";

    // Append the row created to the table body, which will refresh the page
    movieTableBodyElement.append(rowHTML);
}

// Get id from URL
let movieID = getParameterByName('id');

// Makes the HTTP GET request and registers on success callback function handleResult
jQuery.ajax({
    dataType: "json",  // Setting return data type
    method: "GET",// Setting request method
    url: "api/single-movie?id=" + movieID, // Setting request url, which is mapped by StarsServlet in StarsServlet.java
    success: (resultData) => handleResult(resultData) // Setting callback function to handle data returned successfully by the SingleStarServlet
});