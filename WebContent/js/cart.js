$(document).ready(function (event) {
    let currentRow;
    let allMoviesPriceInCart = 0;
    init(addActionOnDecreaseAndDecrease);

    function handleAllMoviesinCart(movie,price){
        let qty = parseInt(movie["qty"],10)
        let totalPrice = price * qty;
        allMoviesPriceInCart += totalPrice;

        let rowHTML = "";
        rowHTML += `<tr data-name="${movie["movie_name"]}" data-id="${movie["movie_id"]}"><td>`;
        rowHTML += '<a href=' + "single-movie.html?id=" + movie["movie_id"]
        rowHTML += ` class="text-success"> ${movie["movie_name"]} </a></td> `;
        rowHTML += `<td> <div class="input-group"> <div class="input-group-prepend"> `;
        rowHTML += `<button class="btn btn-outline-primary" type="button" id="decrement">-</button></div>`;
        rowHTML += `<input type="text" class="form-control text-center" value="${qty}" id="quantity">`;
        rowHTML += `<div class="input-group-append"><button class="btn btn-outline-primary" type="button" id="increment">+</button></div></div>`;
        rowHTML += `<td class="text-center"><button class="btn btn-outline-primary text-center" data-bs-toggle="modal" data-bs-target="#modal-delete" id="cart-remove">DELETE</button></td>`;
        rowHTML += `<td id="eachMoviePrice" class="text-center">$${price}</td>`;
        rowHTML += `<td id="totalPriceEachMovie" class="text-center">$${totalPrice}</td>`;
        rowHTML += `<\tr>`

        $('#cart_tbody').append(rowHTML)
        console.log(allMoviesPriceInCart);
        $('#total-price').html(`$ ${allMoviesPriceInCart}`);
    }

    function addActionOnDecreaseAndDecrease() {
        let moviesInCart = JSON.parse(localStorage.getItem('movies')) || [];
        if (moviesInCart.length !== 0) {
            $('#table-cart').on('click',(e) => {
                if (e.target.tagName.toLowerCase() === 'button') {
                    const btn = e.target;
                    const row = btn.closest('tr');
                    const quantityInput = row.querySelector('#quantity');
                    const eachMoviePrice = parseInt(row.querySelector('#eachMoviePrice').innerHTML.substring(1),10);
                    let currentValue = parseInt(quantityInput.value, 10) || 0;

                    if (btn.id.startsWith('decrement')) {
                        if (currentValue > 0) {
                            allMoviesPriceInCart = allMoviesPriceInCart - (eachMoviePrice)
                            $('#total-price').html(`$ ${allMoviesPriceInCart}`);

                            if (quantityInput.value === "1"){
                                row.remove();
                            }else {
                                row.querySelector('#decrement').classList.remove('disabled')
                                quantityInput.value = currentValue - 1;
                            }
                        }else{
                            row.querySelector('#decrement').classList.add('disabled')
                        }
                    } else if (btn.id.startsWith('increment')) {
                        quantityInput.value = currentValue + 1;
                        allMoviesPriceInCart = allMoviesPriceInCart + (eachMoviePrice)
                        $('#total-price').html(`$ ${allMoviesPriceInCart}`);
                    }
                    //Update the localStorage as Qty changes
                    let movie_name = row.getAttribute("data-name")
                    updateMovieToLocalStorage(movie_name,quantityInput.value)


                    let qty = parseInt(quantityInput.value,10);
                    row.querySelector('#totalPriceEachMovie').innerHTML = "$" + eachMoviePrice * qty + ".00";


                }
            });
        }

        $('#cart_tbody').on('click', '#cart-remove', function () {
            let movie_name = $(this).closest('tr').data('name');
            let movie_id = $(this).closest('tr').data('id');
            // let movie_index = $(this).closest('tr').data('index');

            let modalHeader = document.querySelector("#modal-delete .modal-title");
            modalHeader.innerHTML = "Removing movie from the cart"

            let modalBody = document.querySelector("#modal-delete .modal-body")
            modalBody.innerHTML = `Are you want to delete movie titled <strong>` + movie_name + `<\strong> with ID: <strong>` + movie_id + ` <\strong> ? `

            currentRow = $(this).closest('tr')
        });

        $('#modal-delete-yes').on('click', function (event) {
            if (currentRow){
                currentRow.remove();
                deleteMovieFromLocalStorage(currentRow.data('name'))
                console.log("DELETED");
            }
            $('#modal-delete').modal('hide');
        });

        $('#checkout').on('click', function (event) {
            window.location.href = 'checkout.html';

        });
    }

    function init(addActionOnDecreaseAndDecrease){
        let moviesInCart = JSON.parse(localStorage.getItem('movies')) || [];

        moviesInCart.sort((a, b) => {
            if (a.movie_name < b.movie_name) {
                return -1;
            }
            if (a.movie_name > b.movie_name) {
                return 1;
            }
            return 0;
        });

        if (moviesInCart.length !== 0) {
            $('.inside-container').attr('style','display: none');
            for (let i = 0; i < moviesInCart.length; i++) {
                $('.inside-container').attr('style','display: none')
                fetchMoviePrice(i,moviesInCart[i])
            }
        }

        setTimeout(() =>{
            addActionOnDecreaseAndDecrease();
        },500);
    }

    function fetchMoviePrice(index, movie) {
        $.ajax({
            url: 'api/single-movie-price?',
            method: 'GET',
            data: {
                'id': movie["movie_id"]
            },
            success: function(price) {
                handleAllMoviesinCart(movie,price["price"])
            },
            error: function() {
                console.log("Error fetching price.");
            }
        });
    }

    function deleteMovieFromLocalStorage(movieToBeDelete) {
        let movies = JSON.parse(localStorage.getItem("movies"));

        movies = movies.filter(movie => movie.movie_name !== movieToBeDelete);

        localStorage.setItem("movies", JSON.stringify(movies));
    }

    function updateMovieToLocalStorage(movieToBeUpdated, QtyToBeUpdated){
        let movies = JSON.parse(localStorage.getItem("movies"));

        for (let i = 0; i < movies.length; i++) {
            console.log(movies[i]["movie_name"]);
            if (movies[i]["movie_name"] === movieToBeUpdated) {
                movies[i]["qty"] = QtyToBeUpdated
                localStorage.setItem("movies",JSON.stringify(movies))
                console.log("updated");
                return
            } else{
                console.log("error here updateMovieToLocalStorage");
            }
        }
    }
});