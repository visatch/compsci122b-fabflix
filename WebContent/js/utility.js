export function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function handleLookup(query, doneCallback) {
    console.log("autocomplete initiated")
    console.log("sending AJAX request to backend Java Servlet")
    console.log(query);

    // TODO: if you want to check past query results first, you can do it here
    let retrievedString = localStorage.getItem(query.trim())

    if (retrievedString){
        try{
            let retrievedArray = JSON.parse(retrievedString)

            doneCallback( { suggestions: retrievedArray } );

        }catch (error){
            console.log("ERROR WHILE PARSING THE JSON");
        }
    } else{
        $.ajax({
            url: "/api/search-by-keys",
            method: "GET",
            data: {
                'title': query,
                'limit': 10
            },
            success: function(data) {
                // pass the data, query, and doneCallback function into the success handler
                handleLookupAjaxSuccess(data, query, doneCallback)
            },
            error: function(errorData) {
                console.log("lookup ajax error")
                console.log(errorData)
            }
        })
    }
}

export function handleLookupAjaxSuccess(data, query, doneCallback) {
    console.log("lookup ajax successful")

    // parse the string into JSON
    var jsonData = []

    for (let i = 0; i < data.length-1; i++) {
        let eachMovie = {"value":data[i]["movie_name"],"data":data[i]["movie_id"]};
        // jsonData.suggestions.push(eachMovie);
        jsonData.push(eachMovie)
    }

    console.log(jsonData);

    // TODO: if you want to cache the result into a global variable you can do it here
    if (data.length > 0){
        let jsonString = JSON.stringify(jsonData)
        localStorage.setItem(query,jsonString)

        let search_keys = localStorage.getItem("search-keys")
        let search_keys_json = search_keys ? JSON.parse(search_keys) : [];
        search_keys_json.push(query)
        localStorage.setItem("search-keys",JSON.stringify(search_keys_json))
    }

    // call the callback function provided by the autocomplete library
    // add "{suggestions: jsonData}" to satisfy the library response format according to
    //   the "Response Format" section in documentation
    doneCallback( { suggestions: jsonData } );
}


export function handleSelectSuggestion(suggestion) {
    // TODO: jump to the specific result page based on the selected suggestion
    console.log("you select " + suggestion["value"] + " with ID " + suggestion["data"])
    window.location.href = `single-movie.html?id=${suggestion["data"]}`;
}

export function removeAllSearchKeyFromLocalStorage(){
    let search_keys = localStorage.getItem("search-keys")
    let search_keys_json = search_keys ? JSON.parse(search_keys) : [];

    search_keys_json.forEach(function (key) {
        localStorage.removeItem(key)
    });

    localStorage.removeItem("search-keys")
}
