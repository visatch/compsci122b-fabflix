import {getCookie, handleLookup, handleLookupAjaxSuccess, handleSelectSuggestion, removeAllSearchKeyFromLocalStorage} from "./utility.js";


$(document).ready(function(){
    // localStorage.clear();
    localStorage.removeItem("current-page")
    localStorage.removeItem("moviesPerPage")
    localStorage.removeItem("total-pages")



    let genres = []

    $.ajax({
        url: 'api/get-all-genres',
        method: 'GET',
        success: function (data) {
            for (let i = 0; i < data.length; i++) {
                genres.push(data[i]["genre_name"]);
            }
            genres.sort();
            for (let i = 0; i < genres.length; i++) {
                let g = genres[i]
                const genreDiv = `
                    <div class="col-md-3 h3">
                        <a class="link-success text-decoration-none" href="browse.html?genre=${g}">${g}</a>
                    </div>
                `;
                // Append the created div to the container
                $("#genre-container").append(genreDiv);
            }
        },
        error: function () {
            console.log("ERROR GETTING ALL GENRE. CHECK API AND DB!!!")
        },
    })

    // Arrays of characters and numbers
    const characters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    const numbers = ["0","1", "2", "3", "4", "5", "6", "7", "8", "9", "*"];

    // Iterate through each item in the merged array
    $.each(characters, function(index, item){
        // Create a div structure for each item
        const itemDiv = `
                    <a class="h4" href="browse.html?title=${item}">${item}</a>
                `;
        // Append the created div to the container
        $("#browsing_by_movies_title_alpha").append(itemDiv);
    });

    $.each(numbers, function(index, item){
        // Create a div structure for each item
        const itemDiv = `
                    <a class="h4" href="browse.html?title=${item}">${item}</a>
                `;
        // Append the created div to the container
        $("#browsing_by_movies_title_number").append(itemDiv);
    });

    $('#search').click(function (event) {
        event.preventDefault();

        const title = ($('#title').val() != null) ? $('#title').val() : null  ;
        const year = ($('#year').val() != null) ? $('#year').val() : null;
        const director = ($('#director').val() != null) ? $('#director').val() : null;
        const star = ($('#star').val() != null) ? $('#star').val() : null;
        console.log("sth");
        window.location.href = `browse.html?title=${title}&year=${year}&director=${director}&star=${star}`;

    });

    $('#title').autocomplete({
        // documentation of the lookup function can be found under the "Custom lookup function" section
        lookup: function (query, doneCallback) {
            handleLookup(query,doneCallback)
            console.log("lookup");

        },
        onSelect: function(suggestion) {
            console.log("onSelect");
            handleSelectSuggestion(suggestion);
        },
        // set delay time
        deferRequestBy: 500,
        minChars: 3,
        noCache: false
        // there are some other parameters that you might want to use to satisfy all the requirements
        // TODO: add other parameters, such as minimum characters
    });
});

