/**
 * Handles the data returned by the API, read the jsonObject and populate data into html elements
 * @param resultData jsonObject
 */
function handleAllMovies(resultData) {
    // Populate the star table
    // Find the empty table body by id "star_table_body"

    let allMoviesTableBodyElement = jQuery("#result_movie_tbody");
    let moviesPerPage = $('#moviesPerPage').val();

    let numbersMoviesShow;
    if (localStorage.getItem("current-page") == null || localStorage.getItem("current-page") === "1")
        numbersMoviesShow = Math.min(moviesPerPage,resultData.length -1)
    else
        numbersMoviesShow= Math.min(moviesPerPage,resultData.length)

    // for (let i = 0; i < numbersMoviesShow-1; i++) {
    for (let i = 0; i < moviesPerPage; i++) {
        // Concatenate the html tags with resultData jsonObject
        let rowHTML = "";
        rowHTML += `<tr data-name="${resultData[i]["movie_name"]}"  data-id="${resultData[i]["movie_id"]}">`;
        rowHTML += "<td>" + '<a href=' + "single-movie.html?id=" + resultData[i]["movie_id"] + ' class="text-success" >'
            + resultData[i]["movie_name"] +     // display star_name for the link text
            '</a>' + "</td>";

        rowHTML += "<td>" + resultData[i]["movie_year"] + "</td>";
        rowHTML += "<td>" + resultData[i]["movie_director"] + "</td>";

        rowHTML += "<td>"

        for (let j = 0; j < resultData[i]["movie_genres"].length; j++) {
            let genre_name = resultData[i]["movie_genres"][j]["genre_name"]
            let genre_id = resultData[i]["movie_genres"][j]["genre_id"]
            // Add a link to single-star.html with id passed with GET url parameter
            rowHTML += '<a href=browse.html?genre=' + genre_id + ' class="text-success" >'
                + genre_name +     // display star_name for the link text
                '</a>'

            if (j < resultData[i]["movie_genres"].length - 1)
                rowHTML += ", "
        }
        rowHTML += "</th>";

        rowHTML += "<th>"
        for (let j = 0; j < resultData[i]["movie_stars"].length; j++) {
            let star_name = resultData[i]["movie_stars"][j]["star_name"]
            let star_id = resultData[i]["movie_stars"][j]["star_id"]
            // Add a link to single-star.html with id passed with GET url parameter
            rowHTML += '<a href=single-star.html?id=' + star_id + ' class="text-success" >'
                + star_name +     // display star_name for the link text
                '</a>'

            if (j < resultData[i]["movie_stars"].length - 1)
                rowHTML += ", "
        }
        rowHTML += "</td>";

        rowHTML += "<td>" + resultData[i]["movie_rating"] +
            "<i class=\"fa-regular fa-star fa-beat-fade ms-2\" style=\"color: #118be8;\"></i>";
        rowHTML += `<td><button class="btn btn-outline-primary" id="btn-row-action">ADD</button></td>`;

        rowHTML += "</td></tr>";

        // Append the row created to the table body, which will refresh the page
        allMoviesTableBodyElement.append(rowHTML);
    }

    moviesPerPage = $('#moviesPerPage').val()
    localStorage.setItem("moviesPerPage",moviesPerPage)

    let totalPages = localStorage.getItem("total-pages")
    if (totalPages == null){
        let getTotalPageNumber = resultData[resultData.length-1]["total_pages"]
        totalPages = Math.ceil(getTotalPageNumber / parseInt(moviesPerPage))
        localStorage.setItem("total-pages",String(totalPages))
    }

    let currentPageNumber = localStorage.getItem("current-page");
    if (currentPageNumber == null){
        localStorage.setItem("current-page","1")
        currentPageNumber = 1
    } else{
        currentPageNumber = parseInt(currentPageNumber,10)
    }

    if (currentPageNumber > 1){
        $('#btn-prev').removeClass('disabled')
    } else{
        $('#btn-prev').addClass('disabled')
    }

    if (currentPageNumber >= totalPages){
        $('#btn-next').addClass('disabled')
    } else{
        $('#btn-next').removeClass('disabled')
    }

}

function getParameterByName(target) {
    // Get request URL
    let url = window.location.href;
    // Encode target parameter name to url encoding
    target = target.replace(/[\[\]]/g, "\\$&");

    // Ues regular expression to find matched parameter value
    let regex = new RegExp("[?&]" + target + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';

    // Return the decoded parameter value
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// Makes the HTTP GET request and registers on success callback function handleStarResult
// Loading Indicator
document.getElementById('loading').style.display = 'block';
document.getElementById('pagination-buttons').style.display = 'none';

// Parse URL parameters
const urlParams = new URLSearchParams(window.location.search);

// Decide the API endpoint based on the URL and its parameters

// Get current URL's path
const path = window.location.href;
let apiUrl = "";
function getAPIURL(){
    if (path.includes('genre')) {
        apiUrl = `api/browse-by-genre`;
    } else if (path.includes('title') && !path.includes('year')){
        apiUrl = 'api/browse-by-title'
    } else if (path.includes('all-movie')) {
        apiUrl = 'api/all-movies';
    } else if (path.includes('title') && path.includes('year') && path.includes('director') && path.includes('star')){
        apiUrl = 'api/search-by-keys'
    } else {
        apiUrl = "api/default-all-movies";
    }
    return apiUrl
}

function getPageNumber(){
    return (localStorage.getItem('current-page') != null) ? localStorage.getItem('current-page') : 1;
}

function getApiParams(url) {
    const baseParams = {
        'page': getPageNumber(),
        'limit': $('#moviesPerPage').val(),
        'sort': $('#sortBy').val()
    };

    const getValueOrDefault = (param, defaultValueSelector) => {
        return getParameterByName(param) || $(defaultValueSelector).val();
    };

    console.log(url)


    switch (url) {
        case 'api/browse-by-genre':
            return { ...baseParams, 'genre': getParameterByName('genre') };
        case 'api/browse-by-title':
            return { ...baseParams, 'title': getParameterByName('title') };
        case 'api/search-by-keys':
            return {
                ...baseParams,
                'title': getValueOrDefault('title', '#title'),
                'director': getValueOrDefault('director', '#director'),
                'star': getValueOrDefault('star', '#star'),
                'year': getValueOrDefault('year', '#year')
            };
        default:
            return baseParams;
    }
}

$(document).ready(function (){
    defaultMovieTable()

    let currentPage = (localStorage.getItem('current-page') != null) ? localStorage.getItem('current-page') : 1
    // let totalPages = (localStorage.getItem('total-pages') != null) ? localStorage.getItem('total-pages') : 1

    $('#btn-next-href').click(function (event){
        event.preventDefault();
        currentPage++;
        localStorage.setItem('current-page',currentPage)
        $('#result_movie_tbody').empty()
        document.getElementById('loading').style.display = 'block';
        document.getElementById('pagination-buttons').style.display = 'none';
        $('#page-number').val(currentPage);
        fetchData(currentPage);

    })

    $('#btn-prev-href').click(function (event) {
        event.preventDefault();
        currentPage--;
        localStorage.setItem('current-page',currentPage)
        $('#result_movie_tbody').empty()
        document.getElementById('loading').style.display = 'block';
        document.getElementById('pagination-buttons').style.display = 'none';
        $('#page-number').val(currentPage);
        fetchData(currentPage);
    });

    $('#btn-update').click(function (event) {
        event.preventDefault();
        $('#result_movie_tbody').empty()
        document.getElementById('loading').style.display = 'block';
        document.getElementById('pagination-buttons').style.display = 'none';
        defaultMovieTable();
    });

    $('#table-result').on('click', '#btn-row-action', function () {
        let movies_name = $(this).closest('tr').data('name');
        let movie_id = $(this).closest('tr').data('id');
        alert("Success")
        storeMovie(movie_id, movies_name)
    });

    $('#search').click(function (event) {
        event.preventDefault();

        const title = ($('#title').val() != null) ? $('#title').val() : null  ;
        const year = ($('#year').val() != null) ? $('#year').val() : null;
        const director = ($('#director').val() != null) ? $('#director').val() : null;
        const star = ($('#star').val() != null) ? $('#star').val() : null;
        console.log("sth");
        window.location.href = `browse.html?title=${title}&year=${year}&director=${director}&star=${star}`;

    });

    function storeMovie(movie_id, movies_name) {
        // Retrieve existing movies from localStorage
        let movies = JSON.parse(localStorage.getItem('movies')) || [];

        // Check if movie already exists
        let existingMovie = movies.find(movie => movie.movie_name === movies_name);

        if (existingMovie) {
            // If movie exists, increase its qty by 1
            existingMovie.qty += 1;
        } else {
            // If movie doesn't exist, add it with qty set to 1
            movies.push({ movie_name: movies_name, qty: 1,movie_id: movie_id });
        }

        // Store updated movies back into localStorage
        localStorage.setItem('movies', JSON.stringify(movies));

        console.log(`Stored movie: ${movies_name}. Current Qty: ${existingMovie ? existingMovie.qty : 1}`);
    }

    function fetchData(page){
        let apiURL = getAPIURL();
        $.ajax({
            url: apiURL,
            method: 'GET',
            data: getApiParams(apiURL),
            success: function (data){
                document.getElementById('loading').style.display = 'none';
                document.getElementById('pagination-buttons').style.display = 'inline-block';
                handleAllMovies(data);
            },
            error: function (data) {
                console.log("error when clicking on btn-next-href");
            },
        });
    }

    function defaultMovieTable(){
        jQuery.ajax({
            dataType: "json",
            method: "GET",
            url: getAPIURL(),
            data: getApiParams(apiUrl),
            success: (resultData) => {
                // Hide the loading indicator when data is received
                document.getElementById('loading').style.display = 'none';
                document.getElementById('pagination-buttons').style.display = 'inline-block';

                // Handle the received data
                handleAllMovies(resultData);
            },
            error: (jqXHR, textStatus, errorThrown) => {
                // Hide the loading indicator in case of an error
                document.getElementById('loading').style.display = 'none';

                // Handle or log the error, if needed
                console.error(textStatus, errorThrown);
            }
        });
    }
});


