/**
 * This example is following frontend and backend separation.
 *
 * Before this .js is loaded, the html skeleton is created.
 *
 * This .js performs two steps:
 *      1. Use jQuery to talk to backend API to get the json data.
 *      2. Populate the data to correct html elements.
 */


/**
 * Handles the data returned by the API, read the jsonObject and populate data into html elements
 * @param resultData jsonObject
 */
function handleResult(resultData) {
    console.log("handleStarResult: populating star table from resultData");

    // Populate the star table
    // Find the empty table body by id "star_table_body"
    let starTableBodyElement = jQuery("#top20-movies-tbody");

    // Iterate through resultData, no more than 20 entries
    for (let i = 0; i < Math.min(20, resultData.length); i++) {
        // Concatenate the html tags with resultData jsonObject
        let rowHTML = "";
        rowHTML += "<tr>";
        rowHTML += "<th>" + '<a href=single-movie.html?id=' + resultData[i]["movie_id"] + ' class="text-primary" >'
            + resultData[i]["movie_name"] + '</a>' + "</th>";
        console.log(rowHTML)
        rowHTML += "<th>" + resultData[i]["movie_year"] + "</th>";
        rowHTML += "<th>" + resultData[i]["movie_director"] + "</th>";
        rowHTML += "<th>" + resultData[i]["movie_genres"] + "</th>";

        rowHTML += "<th>"
        for (let j = 0; j < resultData[i]["movie_stars"].length; j++) {
            let star_name = resultData[i]["movie_stars"][j]["star_name"]
            let star_id = resultData[i]["movie_stars"][j]["star_id"]
            // Add a link to single-star.html with id passed with GET url parameter
            rowHTML += '<a href=single-star.html?id=' + star_id + ' class="text-success" >'
                + star_name +     // display star_name for the link text
                '</a>'

            if (j < resultData[i]["movie_stars"].length - 1)
                rowHTML += ", "
        }
        rowHTML += "</th>";

        rowHTML += "<th>" + resultData[i]["movie_rating"] +
            "<i class=\"fa-regular fa-star fa-beat-fade ms-2\" style=\"color: #118be8;\"></i>" + "</th>";
        rowHTML += "</tr>";

        // Append the row created to the table body, which will refresh the page
        starTableBodyElement.append(rowHTML);
    }
}


/**
 * Once this .js is loaded, following scripts will be executed by the browser
 */

// Makes the HTTP GET request and registers on success callback function handleStarResult
jQuery.ajax({
    dataType: "json", // Setting return data type
    method: "GET", // Setting request method
    url: "api/top20-movies", // Setting request url, which is mapped by StarsServlet in Stars.java
    success: (resultData) => handleResult(resultData) // Setting callback function to handle data returned successfully by the StarsServlet
});
