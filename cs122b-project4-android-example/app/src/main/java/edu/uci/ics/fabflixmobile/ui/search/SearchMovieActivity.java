package edu.uci.ics.fabflixmobile.ui.search;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;


import edu.uci.ics.fabflixmobile.R;
import edu.uci.ics.fabflixmobile.data.NetworkManager;
import edu.uci.ics.fabflixmobile.databinding.SearchBoxBinding;
import edu.uci.ics.fabflixmobile.ui.login.LoginActivity;
import edu.uci.ics.fabflixmobile.ui.movielist.MovieListActivity;

public class SearchMovieActivity extends AppCompatActivity{
    public EditText title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SearchBoxBinding binding = SearchBoxBinding.inflate(getLayoutInflater());
        // upon creation, inflate and initialize the layout
        setContentView(binding.getRoot());

        //adjust fabflix logo position
        ImageView myImageView = findViewById(R.id.fabflixImage);
        myImageView.setY(300);
        myImageView.setX(250);

        final Button searchButton = findViewById(R.id.search);

        // initialize the activity(page)/destination
        Intent intent = new Intent(SearchMovieActivity.this, MovieListActivity.class);
        //assign a listener to call a function to handle the user request when clicking a button
        searchButton.setOnClickListener(view -> {
            title = findViewById(R.id.movieTitle);
            intent.putExtra("SEARCH_KEYWORD", title.getText().toString());
            startActivity(intent);
        });
    }
}
