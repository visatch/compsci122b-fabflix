package edu.uci.ics.fabflixmobile.ui.SingleMovie;
import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uci.ics.fabflixmobile.R;
import edu.uci.ics.fabflixmobile.data.NetworkManager;
import edu.uci.ics.fabflixmobile.data.model.Movie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SingleMovieActivity extends AppCompatActivity{
    //    private final String host = "10.0.2.2";
//    private final String port = "8080";
//    private final String baseURL = "http://" + host + ":" + port;
    private final String host = "visatch.dev";
    private final String baseURL = "http://" + host;
    private String title;
    private String year;
    private String rating;
    private String director;
    private String genres;
    private String stars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singlemovie);
        // Todo this should be retrieved from the backend server
        // Remove the title (action bar)
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        getMovie();
    }
    public void getMovie(){
        // use the same network queue across our application
        final RequestQueue queue = NetworkManager.sharedManager(this).queue;
        // get the search keyword from previous intent
        Intent intent = getIntent();
        String receivedKeyword = intent.getStringExtra("SEARCH_KEYWORD");

        // Append the search keyword to the URL
        String urlWithParams = baseURL + "/api/single-movie?id=" + Uri.encode(receivedKeyword);
        final StringRequest singleMovieRequest = new StringRequest(
                Request.Method.GET,
                urlWithParams,
                response -> {
                    try {
                        JSONArray jsonResponse = new JSONArray(response);
                        Log.d("response", jsonResponse.toString());
                        //Loop through all object in jSON response
                        JSONObject row = jsonResponse.getJSONObject(0);
                        //Check movie
                        if(row.has("movie_title")){
                            title = row.getString("movie_title");
                            TextView titleView = findViewById(R.id.movieTitle);
                            titleView.setText(title);
                        }
                        if(row.has("movie_year")){
                            year = "Year: " + String.valueOf(row.getInt("movie_year"));
                            TextView yearView = findViewById(R.id.movieYear);
                            yearView.setText(year);
                        }
                        if(row.has("movie_rating")){
                            rating = "Rating: " + row.getString("movie_rating");
                            TextView ratingView = findViewById(R.id.movieRating);
                            ratingView.setText(rating);
                        }
                        if(row.has("movie_director")){
                            director = "Director: " + row.getString("movie_director");
                            TextView directorView = findViewById(R.id.movieDirector);
                            directorView.setText(director);
                        }
                        if(row.has("movie_genres")){
                            genres = "Genre: " + row.getString("movie_genres");
                            TextView genreView = findViewById(R.id.movieGenre);
                            genreView.setText(genres);
                        }
                        if(row.has("movie_stars")) {
                            JSONArray movieStars = row.getJSONArray("movie_stars");
                            StringBuilder temp = new StringBuilder();
                            for (int i = 0; i < movieStars.length(); i++) {
                                JSONObject genreJsonObj = movieStars.getJSONObject(i);
                                temp.append(genreJsonObj.getString("star_name"));
                                temp.append(", ");
                            }
                            if(temp.length() > 0) {
                                stars = "Star: " + temp.toString().trim().substring(0, temp.length() - 2);
                            }
                            TextView starView = findViewById(R.id.movieStar);
                            starView.setText(stars);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    // error
                    Log.d("search.error", error.toString());
                }) {
            @Override
            protected Map<String, String> getParams() {
                // POST request form data
                final Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        // important: queue.add is where the login request is actually sent
        queue.add(singleMovieRequest);
    }

}
