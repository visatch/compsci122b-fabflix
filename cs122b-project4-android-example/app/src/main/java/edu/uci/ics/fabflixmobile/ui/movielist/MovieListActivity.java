package edu.uci.ics.fabflixmobile.ui.movielist;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uci.ics.fabflixmobile.R;
import edu.uci.ics.fabflixmobile.data.NetworkManager;
import edu.uci.ics.fabflixmobile.data.model.Movie;
import edu.uci.ics.fabflixmobile.ui.SingleMovie.SingleMovieActivity;
import edu.uci.ics.fabflixmobile.ui.search.SearchMovieActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class MovieListActivity extends AppCompatActivity {

    private final String host = "visatch.dev";
    private final String baseURL = "https://" + host;

    private final ArrayList<Movie> movies = new ArrayList<>();
    private int current_page = 1;
    Button preBtn, nextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movielist);
        // Todo this should be retrieved from the backend server
        // Remove the title (action bar)
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        MovieListViewAdapter adapter = new MovieListViewAdapter(this, movies);
        search(adapter, current_page);
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

        //match the buttons
        preBtn = (Button) findViewById(R.id.btnPrevious);
        nextBtn = (Button) findViewById(R.id.btnNext);
        TextView pageTextView = findViewById(R.id.page);
        pageTextView.setText(String.valueOf(current_page));

        nextBtn.setOnClickListener(view -> {
            current_page++;
            adapter.clear();
            search(adapter,current_page);
            listView.setAdapter(adapter);
            pageTextView.setText(String.valueOf(current_page));
            preBtn.setEnabled(true);
        });

        preBtn.setOnClickListener(view -> {
            if(current_page == 1){
                preBtn.setEnabled(false);
                return;
            }
            current_page--;
            adapter.clear();
            search(adapter,current_page);
            listView.setAdapter(adapter);
            pageTextView.setText(String.valueOf(current_page));
        });


        listView.setOnItemClickListener((parent, view, position, id) -> {
            Movie movie = movies.get(position);
//            @SuppressLint("DefaultLocale") String message = String.format("Clicked on position: %d, name: %s, %d", position, movie.getName(), movie.getYear());
//            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            // initialize the activity(page)/destination
            Intent intent = new Intent(MovieListActivity.this, SingleMovieActivity.class);
            //assign a listener to call a function to handle the user request when clicking a button
            intent.putExtra("SEARCH_KEYWORD", movie.getId());
            startActivity(intent);
        });
    }

    @SuppressLint("SetTextI18n")
    public void search(MovieListViewAdapter adapter, int current_page) {
        // use the same network queue across our application
        final RequestQueue queue = NetworkManager.sharedManager(this).queue;

        // get the search keyword from previous intent
        Intent intent = getIntent();
        String receivedKeyword = intent.getStringExtra("SEARCH_KEYWORD");

        // Append the search keyword to the URL
        String urlWithParams = baseURL + "/api/search-by-keys?page=" + current_page + "&limit=10&sort=0&title=" + Uri.encode(receivedKeyword);
        // request type is POST
        final StringRequest loginRequest = new StringRequest(
                Request.Method.GET,
                urlWithParams,
                response -> {
                    try {
                        JSONArray jsonResponse = new JSONArray(response);
                        //Loop through all object in jSON response
                        for(int i = 0; i < jsonResponse.length(); i++){
                            JSONObject row = jsonResponse.getJSONObject(i);
                            if(row.has("movie_name") && row.has("movie_id") && row.has("movie_year") && row.has("movie_name") && row.has("movie_director") && row.has("movie_rating") && row.has("movie_genres") && row.has("movie_stars")) {
                                String movieTitle = row.getString("movie_name");
                                String movieId = row.getString("movie_id");
                                short movieYear = (short) row.getInt("movie_year");
                                movieTitle += " (" + movieYear + ")";
                                String movieDirector = row.getString("movie_director");
                                String movieRating = row.getString("movie_rating");
                                JSONArray movieGenres = row.getJSONArray("movie_genres");
                                JSONArray movieStars = row.getJSONArray("movie_stars");
                                Movie m = new Movie(movieTitle, movieId, movieYear, movieDirector, movieRating, movieGenres, movieStars);
                                movies.add(m);
                                adapter.notifyDataSetChanged();
                            }
                            if(row.has("total_pages")) {
                                if((current_page*10) >= row.getInt("total_pages")){
                                    nextBtn.setEnabled(false);
                                }else{
                                    nextBtn.setEnabled(true);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    // error
                    Log.d("search.error", error.toString());
                }) {
            @Override
            protected Map<String, String> getParams() {
                // POST request form data
                final Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        // important: queue.add is where the login request is actually sent
        queue.add(loginRequest);
    }
}
















//    public ArrayList<Movie> generatePage(int currentPage){
//        int start = (currentPage - 1)*item_per_page;
//        ArrayList<Movie> temp = new ArrayList<>();
//        if(currentPage == last_page && item_remaining>0){
//            for(int i = start; i < start + item_remaining; i++){
//                temp.add(movies.get(i));
//            }
//        }else{
//            for(int i = start; i < start + item_per_page; i++){
//                temp.add(movies.get(i));
//            }
//        }
//        return temp;
//    }