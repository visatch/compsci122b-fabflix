package edu.uci.ics.fabflixmobile.ui.movielist;

import edu.uci.ics.fabflixmobile.R;
import edu.uci.ics.fabflixmobile.data.model.Movie;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.stream.IntStream;


public class MovieListViewAdapter extends ArrayAdapter<Movie> {
    private final ArrayList<Movie> movies;

    // View lookup cache
    private static class ViewHolder {
        TextView title;
        TextView director;
        TextView rating;
        TextView genres;
        TextView stars;
    }

    public MovieListViewAdapter(Context context, ArrayList<Movie> movies) {
        //predefined layout for each item in the list
        super(context, R.layout.movielist_row, movies);
        this.movies = movies;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the movie item for this position
        Movie movie = movies.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.movielist_row, parent, false);
            //match layout with code
            viewHolder.title = convertView.findViewById(R.id.title);
            viewHolder.director = convertView.findViewById(R.id.director);
            viewHolder.rating = convertView.findViewById(R.id.rating);
            viewHolder.genres = convertView.findViewById(R.id.genres);
            viewHolder.stars = convertView.findViewById(R.id.stars);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.title.setText(movie.getName());
        viewHolder.director.setText(movie.getDirector());
        viewHolder.rating.setText(movie.getRating());

        // Convert the JSONArray to a stream of strings and join them
        String genresName = "";
        try {
            genresName = concatGenreString(movie.getGenres());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        viewHolder.genres.setText(genresName);

        // Convert the JSONArray to a stream of strings and join them
        String starsString = "";
        try {
            starsString = concatStarString(movie.getStars());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        //send to view
        viewHolder.stars.setText(starsString);

        // Return the completed view to render on screen
        return convertView;
    }

    public String concatGenreString(JSONArray genres) throws JSONException {
        StringBuilder temp = new StringBuilder();
        //Loop through all object in jSON response
        for (int i = 0; i < Math.min(3, genres.length()); i++) {
            JSONObject genreJsonObj = genres.getJSONObject(i);
            temp.append(genreJsonObj.getString("genre_name"));
            temp.append(", ");
        }
        return temp.toString().trim().substring(0,temp.length()-2);
    }
    public String concatStarString(JSONArray stars) throws JSONException {
        StringBuilder temp = new StringBuilder();
        //Loop through all object in jSON response
        for (int i = 0; i < Math.min(3, stars.length()); i++) {
            JSONObject genreJsonObj = stars.getJSONObject(i);
            temp.append(genreJsonObj.getString("star_name"));
            temp.append(", ");
        }
        return temp.toString().trim().substring(0,temp.length()-2);
    }
}
