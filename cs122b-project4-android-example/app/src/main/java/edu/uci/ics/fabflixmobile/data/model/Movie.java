package edu.uci.ics.fabflixmobile.data.model;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Movie class that captures movie information for movies retrieved from MovieListActivity
 */
public class Movie {
    private final String name;

    private final String id;

    private final short year;

    private final String director;

    private final String rating;

    private final JSONArray genres;

    private final JSONArray stars;



    public Movie(String name, String id, short year, String director, String rating, JSONArray genres, JSONArray stars) {
        this.name = name;
        this.id = id;
        this.year = year;
        this.director = director;
        this.rating = rating;
        this.genres = genres;
        this.stars = stars;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public short getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    public JSONArray getGenres() {
        return genres;
    }

    public JSONArray getStars() {
        return stars;
    }

    public String getRating() {
        return rating;
    }

}