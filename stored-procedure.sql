USE moviedb;

#1
DROP PROCEDURE IF EXISTS BrowseGenres;
DELIMITER //
CREATE PROCEDURE BrowseGenres(IN p_genre VARCHAR(100), IN p_limit int, IN p_page int, IN p_sort int)
BEGIN
    DECLARE v_sorting_conditions VARCHAR(100) DEFAULT '';
    DECLARE v_query VARCHAR(2000) DEFAULT '
    SELECT m.id as movie_id, m.title, m.year, m.director,
    GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
    GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
    GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
    GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
    r.rating FROM movies m
    JOIN genres_in_movies gim on m.id = gim.movieID
    JOIN genres g on g.id = gim.genreId
    JOIN stars_in_movies sim on m.id = sim.movieId
    JOIN stars s on sim.starId = s.id
    LEFT JOIN ratings r on m.id = r.movieId
    GROUP BY m.id, m.title, m.year, m.director, r.rating ';

    set @offset = (p_page - 1) * p_limit;
    set @s = CONCAT(' LIMIT ', p_limit, ' offset ', @offset);
    #check requirements for sorting
    IF p_sort IS NOT NULL THEN
        SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' ORDER BY ');
        #title ASC, rating DESC
        IF p_sort = 0 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating DESC ');
        #title ASC, rating ASC
        ELSEIF p_sort = 1 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating ');
        #title DESC, rating DESC
        ELSEIF p_sort = 2 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating DESC ');
        #title DESC, rating ASC
        ELSEIF p_sort = 3 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating ');
        #rating ASC, title DESC
        ELSEIF p_sort = 4 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title DESC ');
        #rating ASC, title ASC
        ELSEIF p_sort = 5 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title ');
        #rating DESC, title DESC
        ELSEIF p_sort = 6 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title DESC ');
        #rating DESC, title ASC
        ELSE
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title ');
END IF;
END IF;
    SET v_query = CONCAT(v_query, ' HAVING FIND_IN_SET("',p_genre,'", all_genres) ');
    IF LENGTH(v_sorting_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, v_sorting_conditions, @s);
ELSE
        SET v_query = CONCAT(v_query, @s);
END IF;
    SET @dynamic_sql = v_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END //
DELIMITER ;

-----------------------------------------------------------------------------------------------------------
#2
#Procedure for Browsing Genres All Pages
#test: call BrowseGenresAllPages('Drama');

DROP PROCEDURE IF EXISTS BrowseGenresAllPages;
DELIMITER //
CREATE PROCEDURE BrowseGenresAllPages(IN p_genre VARCHAR(100))
BEGIN
SELECT COUNT(DISTINCT m.id) as 'total_count'
FROM movies m
         JOIN genres_in_movies gim on m.id = gim.movieID
         JOIN genres g on g.id = gim.genreId
         JOIN stars_in_movies sim on m.id = sim.movieId
         JOIN stars s on sim.starId = s.id
         LEFT JOIN ratings r on m.id = r.movieId
WHERE g.name = p_genre;
END //
DELIMITER ;


-----------------------------------------------------------------------------------------------------------

#3
#Procedure for Browsing First Movie Letter including Sorting
#syntax: call BrowseMovieLetter(letter, limit, page, sort);
#	sort = 0: title ASC, rating DESC
# 	sort = 1: title ASC, rating ASC
#	sort = 2: title DESC, rating DESC
#	sort = 3: title DESC, rating ASC
#	sort = 4: rating ASC, title DESC
#	sort = 5: rating ASC, title ASC
#	sort = 6: rating DESC, title DESC
#	sort = 7: rating DESC, title ASC
DROP PROCEDURE IF EXISTS BrowseMovieLetter;
DELIMITER //
CREATE PROCEDURE BrowseMovieLetter(IN p_MovieFirstLetter VARCHAR(100), IN p_limit int, IN p_page int, IN p_sort int)
BEGIN
    DECLARE v_sorting_conditions VARCHAR(100) DEFAULT '';
    DECLARE v_query VARCHAR(2000) DEFAULT '
            SELECT m.id as movie_id, m.title, m.year, m.director,
            GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
            GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
            GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
            GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
            r.rating FROM movies m
            JOIN genres_in_movies gim on m.id = gim.movieID
            JOIN genres g on g.id = gim.genreId
            JOIN stars_in_movies sim on m.id = sim.movieId
            JOIN stars s on sim.starId = s.id
            LEFT JOIN ratings r on m.id = r.movieId';
    set @offset = (p_page - 1) * p_limit;
    set @s = CONCAT(' LIMIT ', p_limit, ' offset ', @offset);
    IF p_MovieFirstLetter = '*' THEN
        SET v_query = CONCAT(v_query, ' WHERE m.title REGEXP "^[^A-Za-z0-9 ]" GROUP BY m.id, m.title, m.year, m.director, r.rating ');
ELSE
        SET v_query = CONCAT(v_query, ' WHERE m.title REGEXP "^', p_MovieFirstLetter,'" GROUP BY m.id, m.title, m.year, m.director, r.rating ');
END IF;
    #check requirements for sorting
    IF p_sort IS NOT NULL THEN
        SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' ORDER BY ');
        #title ASC, rating DESC
        IF p_sort = 0 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating DESC ');
        #title ASC, rating ASC
        ELSEIF p_sort = 1 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating ');
        #title DESC, rating DESC
        ELSEIF p_sort = 2 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating DESC ');
        #title DESC, rating ASC
        ELSEIF p_sort = 3 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating ');
        #rating ASC, title DESC
        ELSEIF p_sort = 4 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title DESC ');
        #rating ASC, title ASC
        ELSEIF p_sort = 5 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title ');
        #rating DESC, title DESC
        ELSEIF p_sort = 6 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title DESC ');
        #rating DESC, title ASC
        ELSE
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title ');
END IF;
END IF;
    IF LENGTH(v_sorting_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, v_sorting_conditions, @s);
ELSE
        SET v_query = CONCAT(v_query, @s);
END IF;
    SET @dynamic_sql = v_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END //
DELIMITER ;




-----------------------------------------------------------------------------------------------------------
#4
#new query for browsing movie BY Letter return Total number of records
#syntax: call BrowseMovieLetterAllPages(letter);
DROP PROCEDURE IF EXISTS BrowseMovieLetterAllPages;
DELIMITER //
CREATE PROCEDURE BrowseMovieLetterAllPages(IN p_MovieFirstLetter VARCHAR(100))
BEGIN
    DECLARE v_query VARCHAR(2000) DEFAULT 'SELECT SUM(numberOfMovies) as total_count FROM(
            SELECT COUNT(DISTINCT m.id) as numberOfMovies, m.id as movie_id, m.title, m.year, m.director,
            GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
            GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
            GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
            GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
            r.rating FROM movies m
            JOIN genres_in_movies gim on m.id = gim.movieID
            JOIN genres g on g.id = gim.genreId
            JOIN stars_in_movies sim on m.id = sim.movieId
            JOIN stars s on sim.starId = s.id
            LEFT JOIN ratings r on m.id = r.movieId';

    IF p_MovieFirstLetter = '*' THEN
        SET v_query = CONCAT(v_query, ' WHERE m.title REGEXP "^[^A-Za-z0-9 ]" GROUP BY m.id, m.title, m.year, m.director, r.rating) as X;');
ELSE
        SET v_query = CONCAT(v_query, ' WHERE m.title REGEXP "^', p_MovieFirstLetter,'" GROUP BY m.id, m.title, m.year, m.director, r.rating) as X;');
END IF;

    SET @dynamic_sql = v_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END //
DELIMITER ;

-----------------------------------------------------------------------------------------------------------
#5
DROP PROCEDURE IF EXISTS DefaultBrowseAllMovies ;
DELIMITER $$
CREATE PROCEDURE DefaultBrowseAllMovies(IN P_LIMIT INT, IN P_PAGE INT)
BEGIN
    DECLARE v_query VARCHAR(2000) DEFAULT 'SELECT m.id as movie_id, m.title, m.year, m.director,
        GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
        GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
        GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
        GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
        r.rating FROM movies m
        JOIN genres_in_movies gim on m.id = gim.movieID
        JOIN genres g on g.id = gim.genreId
        JOIN stars_in_movies sim on m.id = sim.movieId
        JOIN stars s on sim.starId = s.id
        LEFT JOIN ratings r on m.id = r.movieId
        GROUP BY m.id, m.title, m.year, m.director, r.rating' ;

    set @offset = (p_page - 1) * p_limit;
    set @s = CONCAT(' LIMIT ', p_limit, ' offset ', @offset);

    SET v_query = CONCAT(v_query,@s);

    SET @dynamic_sql = v_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END $$
DELIMITER ;

-----------------------------------------------------------------------------------------------------------
#6
DROP PROCEDURE IF EXISTS DefaultBrowseAllMoviesAllPages;
DELIMITER $$
CREATE PROCEDURE DefaultBrowseAllMoviesAllPages()
BEGIN
    -- Query for total count
    DECLARE v_count_query VARCHAR(2000) DEFAULT 'SELECT COUNT(DISTINCT m.id) as total_count FROM movies m
        JOIN genres_in_movies gim on m.id = gim.movieID
        JOIN genres g on g.id = gim.genreId
        JOIN stars_in_movies sim on m.id = sim.movieId
        JOIN stars s on sim.starId = s.id
        LEFT JOIN ratings r on m.id = r.movieId';

    -- Execute paginated results query
    SET @dynamic_sql = v_count_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END $$
DELIMITER ;



-----------------------------------------------------------------------------------------------------------
#7
#Procedure including SearchMovies including sorting
#syntax: call SearchMovies(title, year, director, star, limit, page, sort);
#	sort = 0: title ASC, rating DESC
# 	sort = 1: title ASC, rating ASC
#	sort = 2: title DESC, rating DESC
#	sort = 3: title DESC, rating ASC
#	sort = 4: rating ASC, title DESC
#	sort = 5: rating ASC, title ASC
#	sort = 6: rating DESC, title DESC
#	sort = 7: rating DESC, title ASC

DROP PROCEDURE IF EXISTS SearchMovies;
DELIMITER //
CREATE PROCEDURE SearchMovies(IN p_title VARCHAR(100), IN p_year int, IN p_director varchar(100), IN p_star VARCHAR(100), IN p_limit int, IN p_page int, IN p_sort int)
BEGIN
    DECLARE v_conditions VARCHAR(1000) DEFAULT '';
    DECLARE v_sorting_conditions VARCHAR(100) DEFAULT '';
    DECLARE v_query VARCHAR(2000) DEFAULT 'SELECT m.id as movie_id, m.title, m.year, m.director,
        GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
        GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
        GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
        GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
        r.rating FROM movies m
        JOIN genres_in_movies gim on m.id = gim.movieID
        JOIN genres g on g.id = gim.genreId
        JOIN stars_in_movies sim on m.id = sim.movieId
        JOIN stars s on sim.starId = s.id
        LEFT JOIN ratings r on m.id = r.movieId';

    set @offset = (p_page - 1) * p_limit;
    set @s = CONCAT(' LIMIT ', p_limit, ' offset ', @offset);

    #check requirement for substring title
    IF p_title IS NOT NULL AND LENGTH(p_title) > 0 THEN
        SET v_conditions = CONCAT(v_conditions, ' m.title LIKE ', CONCAT("'",CONCAT('%', p_title ,'%'),"'"));
END IF;

    IF p_year IS NOT NULL THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.year = ', p_year);
END IF;

    IF p_director IS NOT NULL AND LENGTH(p_director) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.director LIKE ',CONCAT("'",CONCAT('%', p_director ,'%'),"'"));
END IF;

    IF p_star IS NOT NULL AND LENGTH(p_star) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
END IF;
        SET v_conditions = CONCAT(v_conditions, ' s.name LIKE ', CONCAT("'",CONCAT('%', p_star ,'%'),"'"));
END IF;
    #check requirements for sorting
    IF p_sort IS NOT NULL THEN
        SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' ORDER BY ');
        #title ASC, rating DESC
        IF p_sort = 0 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating DESC ');
        #title ASC, rating ASC
        ELSEIF p_sort = 1 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating ');
        #title DESC, rating DESC
        ELSEIF p_sort = 2 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating DESC ');
        #title DESC, rating ASC
        ELSEIF p_sort = 3 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating ');
        #rating ASC, title DESC
        ELSEIF p_sort = 4 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title DESC ');
        #rating ASC, title ASC
        ELSEIF p_sort = 5 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title ');
        #rating DESC, title DESC
        ELSEIF p_sort = 6 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title DESC ');
        #rating DESC, title ASC
        ELSE
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title ');
END IF;
END IF;

    IF LENGTH(v_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, ' WHERE', v_conditions);
END IF;

    SET v_query = CONCAT(v_query, ' GROUP BY m.id, m.title, m.year, m.director, r.rating ');

    IF LENGTH(v_sorting_conditions) > 0 THEN
         SET v_query = CONCAT(v_query, v_sorting_conditions, @s);
ELSE
        SET v_query = CONCAT(v_query, @s);
END IF;
    SET @dynamic_sql = v_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END //
DELIMITER ;

-----------------------------------------------------------------------------------------------------------
#8
#new query for searching return counting number of records
#syntax: call SearchMovies_CountRecords(title, year, director, star);
#new query for searching return counting number of records
DROP PROCEDURE IF EXISTS SearchMovies_CountRecords;
DELIMITER //
CREATE PROCEDURE SearchMovies_CountRecords(IN p_title VARCHAR(100), IN p_year int, IN p_director varchar(100), IN p_star VARCHAR(100))
BEGIN
    DECLARE v_conditions VARCHAR(1000) DEFAULT '';
    DECLARE v_query VARCHAR(2000) DEFAULT 'SELECT COALESCE(SUM(numberOfMovies), 0) as total FROM (SELECT COUNT(DISTINCT m.id) as numberOfMovies, m.id as movie_id, m.title, m.year, m.director,
                                                            GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
                                                            GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
                                                            GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
                                                            GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
                                                            r.rating FROM movies m
                                                            JOIN genres_in_movies gim on m.id = gim.movieID
                                                            JOIN genres g on g.id = gim.genreId
                                                            JOIN stars_in_movies sim on m.id = sim.movieId
                                                            JOIN stars s on sim.starId = s.id
                                                            LEFT JOIN ratings r on m.id = r.movieId';

    #check requirement for substring title
    IF p_title IS NOT NULL AND LENGTH(p_title) > 0 THEN
        SET v_conditions = CONCAT(v_conditions, ' m.title LIKE ', CONCAT("'",CONCAT('%', p_title ,'%'),"'"));
END IF;

    IF p_year IS NOT NULL THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.year = ', p_year);
END IF;

    IF p_director IS NOT NULL AND LENGTH(p_director) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.director LIKE ',CONCAT("'",CONCAT('%', p_director ,'%'),"'"));
END IF;

    IF p_star IS NOT NULL AND LENGTH(p_star) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
END IF;
        SET v_conditions = CONCAT(v_conditions, ' s.name LIKE ', CONCAT("'",CONCAT('%', p_star ,'%'),"'"));
END IF;

    IF LENGTH(v_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, ' WHERE', v_conditions);
END IF;

    SET v_query = CONCAT(v_query, ' GROUP BY m.id, m.title, m.year, m.director, r.rating) as X;');

    SET @dynamic_sql = v_query;
PREPARE stmt FROM @dynamic_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
END //
DELIMITER ;

-----------------------------------------------------------------------------------------------------------

CREATE TABLE movie_prices (
                              price DECIMAL(10,2) NOT NULL,
                              movie_ID varchar(10) PRIMARY KEY ,
                              FOREIGN KEY (movie_ID) REFERENCES movies(id)
);

INSERT INTO movie_prices (price, movie_id)
SELECT ROUND(1 + RAND() * 500), id
FROM movies;
-----------------------------------------------------------------------------------------------------------
#Update procedure for all the rest of mysql query in the project

#Procedure for AllMovies.java, query1
DROP PROCEDURE IF EXISTS DisplayAllMovies;
DELIMITER //
CREATE PROCEDURE DisplayAllMovies ()
BEGIN
SELECT A.id, A.title, A.year, A.director, B.rating
FROM movies A
         LEFT JOIN ratings B ON A.id = B.movieId;
END //
DELIMITER ;

-----------------------------------------------------------------------------------------------------------
#Procedure for AllMovies.java file, query2
DROP PROCEDURE IF EXISTS GetGenresFromMovie;
DELIMITER //
CREATE PROCEDURE GetGenresFromMovie (IN p_movieId VARCHAR(10))
BEGIN
SELECT m.id,g.name FROM genres_in_movies
        join genres g on g.id = genres_in_movies.genreId
        join movies m on genres_in_movies.movieID = m.id
where movieID = p_movieId LIMIT 3;
END //
DELIMITER ;

-----------------------------------------------------------------------------------------------------------
#Procedure for AllMovies.java file, query3
DROP PROCEDURE IF EXISTS GetStarsFromMovie;
DELIMITER //
CREATE PROCEDURE GetStarsFromMovie (IN p_movieId VARCHAR(10))
BEGIN
    SELECT sm.movieId, s.name, s.id FROM stars_in_movies sm
    JOIN stars s ON sm.starId = s.id
    JOIN movies m ON m.id = sm.movieId
WHERE movieId = p_movieId
    LIMIT 3;
END //
DELIMITER ;
-----------------------------------------------------------------------------------------------------------
#Procedure for Top20Movies.java, query1
DROP PROCEDURE IF EXISTS GetTop20Movies;
DELIMITER //
CREATE PROCEDURE GetTop20Movies ()
BEGIN
SELECT A.id, A.title, A.year, A.director, B.rating
FROM movies A, ratings B
WHERE A.id = B.movieId
ORDER BY rating DESC
    LIMIT 20;
END //
DELIMITER ;

#Procedure for Top20Movies.java file, query2
DROP PROCEDURE IF EXISTS Top20Movies_GetGenresFromMovieId;
DELIMITER //
CREATE PROCEDURE Top20Movies_GetGenresFromMovieId (IN p_movieId VARCHAR(10))
BEGIN
    SELECT m.id,g.name FROM genres_in_movies
    join genres g on g.id = genres_in_movies.genreId
    join movies m on genres_in_movies.movieID = m.id
    where movieID = p_movieId
    LIMIT 3;
END //
DELIMITER ;

#Procedure for Top20Movies.java file, query3
DROP PROCEDURE IF EXISTS Top20Movies_GetStarsFromMovieId;
DELIMITER //
CREATE PROCEDURE Top20Movies_GetStarsFromMovieId (IN p_movieId VARCHAR(10))
BEGIN
     SELECT sm.movieId, s.name, s.id FROM stars_in_movies sm
     JOIN stars s ON sm.starId = s.id
     JOIN movies m ON m.id = sm.movieId
     WHERE movieId = p_movieId
     LIMIT 3;
END //
DELIMITER ;

#Procedure for StarServlet.java
DROP PROCEDURE IF EXISTS StarsOnly;
DELIMITER //
CREATE PROCEDURE StarsOnly ()
BEGIN
SELECT * from stars;
END //
DELIMITER ;

#Procedure for SingleStarServlet.java
DROP PROCEDURE IF EXISTS SingleStar;
DELIMITER //
CREATE PROCEDURE SingleStar (IN p_starId VARCHAR(10))
BEGIN
SELECT s.id as star_id, s.name, s.birthYear, m.title, m.year, m.director, m.id as movie_id
FROM stars s
         JOIN stars_in_movies sm ON s.id = sm.starId and s.id = p_starId
         JOIN movies m ON sm.movieId = m.id;
END //
DELIMITER ;

#Procedure for SingleMovieServlet
DROP PROCEDURE IF EXISTS SingleMovie;
DELIMITER //
CREATE PROCEDURE SingleMovie (IN p_movieId VARCHAR(10))
BEGIN
    SELECT m.id, m.title, m.year, m.director, r.rating, GROUP_CONCAT(DISTINCT g.name) as all_genres, s.id, s.name
    FROM movies m
             LEFT JOIN ratings r ON m.id = r.movieId
             JOIN genres_in_movies gm ON m.id = gm.movieID
             JOIN genres g ON gm.genreId = g.id
             JOIN stars_in_movies sm ON m.id = sm.movieId
             JOIN stars s ON s.id = sm.starId
    WHERE m.id = p_movieId
    GROUP BY m.id, m.title, m.year, m.director, r.rating, s.id, s.name;
END //
DELIMITER ;

#Procedure for LoginServlet.java
DROP PROCEDURE IF EXISTS Login;
DELIMITER //
CREATE PROCEDURE Login (IN p_email VARCHAR(50), IN p_password VARCHAR(20))
BEGIN
SELECT EXISTS (SELECT * FROM customers WHERE email = p_email AND password = p_password) as result;
END //
DELIMITER ;

#Procedure for getMoviePriceByID.java
DROP PROCEDURE IF EXISTS GetPriceFromMovieID;
DELIMITER //
CREATE PROCEDURE GetPriceFromMovieID (IN p_movieId VARCHAR(10))
BEGIN
SELECT * FROM movie_prices WHERE movie_ID = p_movieId;
END //
DELIMITER ;


#Create Employee Table
CREATE TABLE employees(
  email varchar(50) primary key,
  password varchar(20) not null,
  fullname varchar(100)
);
INSERT INTO employees VALUES('classta@email.edu','classta','TA CS122B');

#procedure for checking employee login
DROP PROCEDURE IF EXISTS CheckEmployeeLogin;
DELIMITER //
CREATE PROCEDURE CheckEmployeeLogin (IN p_email VARCHAR(50), IN p_password VARCHAR(20))
BEGIN
SELECT EXISTS (SELECT * FROM employees WHERE email = p_email AND password = p_password) as result;
END //
DELIMITER ;

#procedure for adding a new movie
DROP PROCEDURE IF EXISTS add_movie;
DELIMITER //
CREATE PROCEDURE add_movie (IN p_id VARCHAR(10), IN p_title VARCHAR(100), IN p_year int, IN p_director VARCHAR(100))
BEGIN
INSERT INTO movies VALUES(p_id, p_title, p_year, p_director);
END //
DELIMITER ;

#procedure to get the last id from movie table
DROP PROCEDURE IF EXISTS getLastMovieId;
DELIMITER //
CREATE PROCEDURE getLastMovieId ()
BEGIN
select max(id) as id from movies;
END //
DELIMITER ;


#Procedure check if the movie exist in db yet?
DROP PROCEDURE IF EXISTS checkMovieInDB;
DELIMITER //
CREATE PROCEDURE checkMovieInDB (IN p_title VARCHAR(100), IN p_year int, IN p_director VARCHAR(100))
BEGIN
SELECT EXISTS (
    SELECT * FROM movies WHERE title = p_title AND year = p_year AND director = p_director
) as result;
END //
DELIMITER ;

#procedure to check if the new star exists in DB
DROP PROCEDURE IF EXISTS checkStarInDB;
DELIMITER //
CREATE PROCEDURE checkStarInDB (IN p_starName VARCHAR(100))
BEGIN
SELECT id
FROM (SELECT * FROM stars WHERE name = p_starName) as result;
END //
DELIMITER ;

#procedure to get the last star from star table
DROP PROCEDURE IF EXISTS getLastStarId;
DELIMITER //
CREATE PROCEDURE getLastStarId ()
BEGIN
select max(id) as id from stars;
END //
DELIMITER ;
---------------------------
#procedure to add a new star into the star table
DROP PROCEDURE IF EXISTS add_star;
DELIMITER //
CREATE PROCEDURE add_star (IN p_starId VARCHAR(10), IN p_starName VARCHAR(100), IN p_birthYear int)
BEGIN
    IF p_birthYear IS NULL OR p_birthYear = '' THEN
        INSERT INTO stars(id, name,birthYear) VALUES(p_starId, p_starName, p_birthYear);
ELSE
        INSERT INTO stars(id, name) VALUES(p_starId, p_starName);
END IF;
END //
DELIMITER ;


#procedure to insert a relationship (starId and movieId) into the stars_in_movies table
DROP PROCEDURE IF EXISTS add_movie_star_relationship;
DELIMITER //
CREATE PROCEDURE add_movie_star_relationship (IN p_starId VARCHAR(10), IN p_movieId VARCHAR(10))
BEGIN
INSERT INTO stars_in_movies VALUES(p_starId, p_movieId);
END //
DELIMITER ;

#procedure to check the genre if it exists in db
DROP PROCEDURE IF EXISTS checkGenreInDB;
DELIMITER //
CREATE PROCEDURE checkGenreInDB (IN p_genreName VARCHAR(32))
BEGIN
SELECT id
FROM (SELECT * FROM genres WHERE name = p_genreName) as result;
END //
DELIMITER ;

#procedure to get the last genre from genre table
DROP PROCEDURE IF EXISTS getLastGenreId;
DELIMITER //
CREATE PROCEDURE getLastGenreId ()
BEGIN
select max(id) as id from genres;
END //
DELIMITER ;

#procedure to add a new genre into the genre table
DROP PROCEDURE IF EXISTS add_genre;
DELIMITER //
CREATE PROCEDURE add_genre (IN p_genreId int, IN p_genreName VARCHAR(32))
BEGIN
INSERT INTO genres VALUES(p_genreId, p_genreName);
END //
DELIMITER ;

#procedure to insert a relationship (genreId and movieID) into the stars_in_movies table
DROP PROCEDURE IF EXISTS add_genre_movie_relationship;
DELIMITER //
CREATE PROCEDURE add_genre_movie_relationship (IN p_genreId int, IN p_movieID VARCHAR(10))
BEGIN
INSERT INTO genres_in_movies VALUES(p_genreId, p_movieID);
END //
DELIMITER ;

#procedure get all genres name
DROP PROCEDURE IF EXISTS get_genres;
DELIMITER //
CREATE PROCEDURE get_genres ()
BEGIN
select name from genres;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS GetUserPasswordByEmail;
DELIMITER //
CREATE PROCEDURE GetUserPasswordByEmail(IN p_email VARCHAR(50))
BEGIN
    SELECT password from customers where email = p_email;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS GetEmployeePwdByEmail;
DELIMITER //
CREATE PROCEDURE GetEmployeePwdByEmail(IN e_email VARCHAR(50))
BEGIN
    SELECT password FROM Employees WHERE email = e_email;
END //

#PROJECT 4 CODE
DROP PROCEDURE IF EXISTS SearchMovies;
DELIMITER //
CREATE PROCEDURE SearchMovies(IN p_title VARCHAR(100), IN p_year int, IN p_director varchar(100), IN p_star VARCHAR(100), IN p_limit int, IN p_page int, IN p_sort int)
BEGIN
    DECLARE v_conditions VARCHAR(1000) DEFAULT '';
    DECLARE v_sorting_conditions VARCHAR(100) DEFAULT '';

    DECLARE v_query VARCHAR(2000) DEFAULT 'SELECT m.id as movie_id, m.title, m.year, m.director,
        GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
        GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
        GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
        GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
        r.rating FROM movies m
        JOIN genres_in_movies gim on m.id = gim.movieID
        JOIN genres g on g.id = gim.genreId
        JOIN stars_in_movies sim on m.id = sim.movieId
        JOIN stars s on sim.starId = s.id
        LEFT JOIN ratings r on m.id = r.movieId';

    DECLARE i, word_count INT DEFAULT 0;
    DECLARE word VARCHAR(255);
    SET word_count = CountWords(p_title);

    SET @offset = (p_page - 1) * p_limit;
    SET @s = CONCAT(' LIMIT ', p_limit, ' offset ', @offset);

    IF p_title IS NOT NULL AND LENGTH(p_title) > 0 THEN
        WHILE i < word_count DO
                SET i = i + 1;
                SET word = SPLIT_STR(p_title, ' ', i);
                IF LENGTH(v_conditions) > 0 THEN
                    SET v_conditions = CONCAT(v_conditions, ' AND ');
                END IF;
                SET v_conditions = CONCAT(v_conditions, ' m.title LIKE \'%', word, '%\'');
            END WHILE;
    END IF;

    IF p_year IS NOT NULL THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
        END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.year = ', p_year);
    END IF;

    IF p_director IS NOT NULL AND LENGTH(p_director) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
        END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.director LIKE ',CONCAT("'",CONCAT('%', p_director ,'%'),"'"));
    END IF;

    IF p_star IS NOT NULL AND LENGTH(p_star) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
        END IF;
        SET v_conditions = CONCAT(v_conditions, ' s.name LIKE ', CONCAT("'",CONCAT('%', p_star ,'%'),"'"));
    END IF;

    #check requirements for sorting
    IF p_sort IS NOT NULL THEN
        SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' ORDER BY ');
        #title ASC, rating DESC
        IF p_sort = 0 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating DESC ');
            #title ASC, rating ASC
        ELSEIF p_sort = 1 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title, r.rating ');
            #title DESC, rating DESC
        ELSEIF p_sort = 2 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating DESC ');
            #title DESC, rating ASC
        ELSEIF p_sort = 3 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' m.title DESC, r.rating ');
            #rating ASC, title DESC
        ELSEIF p_sort = 4 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title DESC ');
            #rating ASC, title ASC
        ELSEIF p_sort = 5 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating, m.title ');
            #rating DESC, title DESC
        ELSEIF p_sort = 6 THEN
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title DESC ');
            #rating DESC, title ASC
        ELSE
            SET v_sorting_conditions = CONCAT(v_sorting_conditions, ' r.rating DESC, m.title ');
        END IF;
    END IF;

    IF LENGTH(v_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, ' WHERE', v_conditions);
    END IF;

    SET v_query = CONCAT(v_query, ' GROUP BY m.id, m.title, m.year, m.director, r.rating ');

    IF LENGTH(v_sorting_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, v_sorting_conditions, @s);
    ELSE
        SET v_query = CONCAT(v_query, @s);
    END IF;
    SET @dynamic_sql = v_query;

    PREPARE stmt FROM @dynamic_sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //
DELIMITER ;

#SPLIT STRING
DELIMITER $$
DROP FUNCTION IF EXISTS SPLIT_STR;
CREATE FUNCTION SPLIT_STR(
    x VARCHAR(255),
    delim VARCHAR(12),
    pos INT
) RETURNS VARCHAR(255)
    DETERMINISTIC
    NO SQL
BEGIN
    RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
                             LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
                   delim, '');
END$$
DELIMITER ;

# Count words
DELIMITER $$
DROP FUNCTION IF EXISTS CountWords;
CREATE FUNCTION CountWords(str VARCHAR(255))
    RETURNS INT
    DETERMINISTIC
    NO SQL
BEGIN
    DECLARE word_count, idx INT;
    SET word_count = 0;
    SET idx = 1;

    IF LENGTH(str) - LENGTH(REPLACE(str, ' ', '')) > 0 THEN
        WHILE idx <= LENGTH(str) - LENGTH(REPLACE(str, ' ', '')) + 1 DO
                SET word_count = word_count + 1;
                SET idx = idx + 1;
            END WHILE;
    ELSE
        SET word_count = 1;
    END IF;

    RETURN word_count;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS SearchMovies_CountRecords;
DELIMITER //
CREATE PROCEDURE SearchMovies_CountRecords(IN p_title VARCHAR(100), IN p_year int, IN p_director varchar(100), IN p_star VARCHAR(100))
BEGIN
    DECLARE v_conditions VARCHAR(1000) DEFAULT '';
    DECLARE v_query VARCHAR(2000) DEFAULT 'SELECT COALESCE(SUM(numberOfMovies), 0) as total_count FROM (SELECT COUNT(DISTINCT m.id) as numberOfMovies, m.id as movie_id, m.title, m.year, m.director,
                                                            GROUP_CONCAT(DISTINCT g.name ORDER BY g.name) as all_genres,
                                                            GROUP_CONCAT(DISTINCT s.name ORDER BY s.name) as all_stars,
                                                            GROUP_CONCAT(DISTINCT s.id ORDER BY s.name) as all_stars_id,
                                                            GROUP_CONCAT(DISTINCT g.id ORDER BY g.name) as all_genres_id,
                                                            r.rating FROM movies m
                                                            JOIN genres_in_movies gim on m.id = gim.movieID
                                                            JOIN genres g on g.id = gim.genreId
                                                            JOIN stars_in_movies sim on m.id = sim.movieId
                                                            JOIN stars s on sim.starId = s.id
                                                            LEFT JOIN ratings r on m.id = r.movieId';
    DECLARE i, word_count INT DEFAULT 0;
    DECLARE word VARCHAR(255);
    SET word_count = CountWords(p_title);

    #check requirement for substring title
    IF p_title IS NOT NULL AND LENGTH(p_title) > 0 THEN
        WHILE i < word_count DO
                SET i = i + 1;
                SET word = SPLIT_STR(p_title, ' ', i);
                IF LENGTH(v_conditions) > 0 THEN
                    SET v_conditions = CONCAT(v_conditions, ' AND ');
                END IF;
                SET v_conditions = CONCAT(v_conditions, ' m.title LIKE \'%', word, '%\'');
            END WHILE;
    END IF;

    IF p_year IS NOT NULL THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
        END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.year = ', p_year);
    END IF;

    IF p_director IS NOT NULL AND LENGTH(p_director) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
        END IF;
        SET v_conditions = CONCAT(v_conditions, ' m.director LIKE ',CONCAT("'",CONCAT('%', p_director ,'%'),"'"));
    END IF;

    IF p_star IS NOT NULL AND LENGTH(p_star) > 0 THEN
        IF LENGTH(v_conditions) > 0 THEN
            SET v_conditions = CONCAT(v_conditions, ' AND');
        END IF;
        SET v_conditions = CONCAT(v_conditions, ' s.name LIKE ', CONCAT("'",CONCAT('%', p_star ,'%'),"'"));
    END IF;

    IF LENGTH(v_conditions) > 0 THEN
        SET v_query = CONCAT(v_query, ' WHERE', v_conditions);
    END IF;

    SET v_query = CONCAT(v_query, ' GROUP BY m.id, m.title, m.year, m.director, r.rating) as X;');

    SET @dynamic_sql = v_query;
    PREPARE stmt FROM @dynamic_sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //
DELIMITER ;