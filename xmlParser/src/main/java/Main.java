import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        System.out.println("RUNNINGGGG!!!");
        DomMovieParser domMovieParser = new DomMovieParser();

        domMovieParser.run();

        SAXParserStar spe = new SAXParserStar();
        spe.runExample();
        Connection dbCon = null;
        try {
            // Create a new connection to database
            String url = "jdbc:mysql://localhost:3306/moviedb";
            String user = "mytestuser";
            String password = "teamchocolate";
            dbCon = DriverManager.getConnection(url, user, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        spe.createFile();
        spe.writeToFile(dbCon);
        spe.insertStarIntoDB(dbCon);
        SAXParserStarMovieRelation insertRelation = new SAXParserStarMovieRelation();
        insertRelation.runExample();
        insertRelation.createFile();
        insertRelation.writeToFile();
        insertRelation.insertStarMovieRelationIntoDB();

        System.out.println("Inserted stars: " + GlobalVariables.count_inserted_star);
        System.out.println("Inserted stars_in_movies: " + GlobalVariables.count_inserted_stars_in_movies);
        System.out.println("Movies has no star: "+ GlobalVariables.count_star_notFound);
        System.out.println("Stars duplicate: " + GlobalVariables.count_star_duplicate);
        System.out.println("Stars not Found: " + GlobalVariables.count_star_notFound);
    }
}
