public class Star {
    private String name;
    private String birthyear;

    private String starId;
    public Star(){
    }

    public Star(String starId, String name, String birthyear) {
        this.starId = starId;
        this.name = name;
        this.birthyear = birthyear;
    }

    public String getStarId(){
        return starId;
    }
    public void setStarId(String starId){
        this.starId = starId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.isEmpty()){
            GlobalVariables.count_star_notFound ++;
        }
        this.name = name;
    }
    public String getBirthYear() {
        return birthyear;
    }
    public void setBirthYear(String birthyear) {
        this.birthyear = birthyear;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Star Details - ");
        sb.append("Name:" + getName());
        sb.append(", ");
        sb.append("Birth year:" + getBirthYear());
        return sb.toString();
    }
}
