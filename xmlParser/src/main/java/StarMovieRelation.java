public class StarMovieRelation {
    private String movieId;
    private String starId;

    private String starName;
    public StarMovieRelation(){
    }

    public StarMovieRelation(String movieId, String starId, String starName) {
        this.movieId = movieId;
        this.starId = starId;
        this.starName = starName;
    }

    public String getMovieId() {
        return movieId;
    }
    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }
    public String getStarId(){
        return starId;
    }
    public void setStarId(String starId){
        this.starId = starId;
    }
    public String getStarName(){
        return starName;
    }
    public void setStarName(String starName){
        this.starName = starName;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Movie-Star Details - ");
        sb.append("MovieId:" + getMovieId());
        sb.append(", ");
        sb.append("StarId:" + getStarId());
        return sb.toString();
    }
}

