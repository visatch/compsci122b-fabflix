import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class SAXParserStar extends DefaultHandler {
    public SAXParserStar() {
        GlobalVariables.newStars = new ArrayList<>();
    }

    public void runExample() {
        parseDocument();
    }

    private void parseDocument() {
        //get a factory
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            //get a new instance of parser
            SAXParser sp = spf.newSAXParser();
            //parse the file and also register this class for call backs
            sp.parse("actors63.xml", this);
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }


    //Event Handlers
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //reset
        GlobalVariables.tempVal = "";
        if (qName.equalsIgnoreCase("actor")) {
            //create a new instance of star
            GlobalVariables.tempStar = new Star();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
            GlobalVariables.tempVal = new String(ch, start, length);
    }


    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("actor")) {
            //add it to the list
            GlobalVariables.newStars.add(GlobalVariables.tempStar);
        } else if (qName.equalsIgnoreCase("stagename")) {
            GlobalVariables.tempStar.setName(GlobalVariables.tempVal);
        } else if (qName.equalsIgnoreCase("dob")) {
            GlobalVariables.tempStar.setBirthYear(GlobalVariables.tempVal);
        }
    }

    public void createFile() {
        File myObj = new File("stars.sql");
        try {
            boolean newFile = myObj.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToFile(Connection dbCon){
        try {
            FileWriter myWriter = new FileWriter("stars.sql");
            //Generate new star id
            String nextStarId = generateNewStarID(dbCon);
            boolean firstId = true;
            myWriter.write("use moviedb;\n");
            //writing into the file
            for (Star newStar : GlobalVariables.newStars) {
                String starName = newStar.getName();
                String birthYear;
                if(!firstId) {
                    //continue generate new id
                    String sub = nextStarId.substring(2);
                    int x = Integer.parseInt(sub) + 1;
                    nextStarId = "nm" + x;
                }
                firstId = false;
                if(GlobalVariables.starHashMap.containsKey(starName)){
                    GlobalVariables.count_star_duplicate ++;
                }
                GlobalVariables.starHashMap.put(starName, nextStarId);
                if (newStar.getBirthYear() != null && newStar.getBirthYear().isEmpty()) {
                    birthYear = "0";
                }else if (newStar.getBirthYear() == null) {
                    birthYear = "0";
                }else{
                    if(!isNumeric(newStar.getBirthYear().trim())){
                        birthYear = "0";
                    }else {
                        birthYear = newStar.getBirthYear().trim();
                    }
                }

                if(birthYear.equals("0")){
                    if (starName.contains(String.valueOf('\''))){
                        myWriter.write("INSERT INTO stars (id, name) VALUES('" + nextStarId + "',\"" + starName + "\");\n");
                    }else {
                        myWriter.write("INSERT INTO stars (id, name) VALUES('" + nextStarId + "','" + starName + "');\n");
                    }
                }else {
                    if (starName.contains(String.valueOf('\''))){
                        myWriter.write("INSERT INTO stars (id, name, birthYear) VALUES('" + nextStarId + "',\"" + starName + "\"," + birthYear + ");\n");
                    }else {
                        myWriter.write("INSERT INTO stars (id, name, birthYear) VALUES('" + nextStarId + "','" + starName + "'," + birthYear + ");\n");
                    }
                }
            }
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void insertStarIntoDB(Connection dbCon) {
        try {
            // Read SQL file
            BufferedReader reader = new BufferedReader(new FileReader("stars.sql"));
            
            String line;
            // Create PreparedStatement
            PreparedStatement preparedStatement = null;
            
            while ((line = reader.readLine()) != null) {
                GlobalVariables.count_inserted_star ++;
                preparedStatement = dbCon.prepareStatement(line);
                // Execute SQL statements
                preparedStatement.execute();
            }
            // Close resources
            reader.close();
            assert preparedStatement != null;
            preparedStatement.close();
            dbCon.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String generateNewStarID(Connection dbCon) throws SQLException {
        String query = "{CALL getLastStarId()}";
        CallableStatement statement = dbCon.prepareCall(query);
        ResultSet resultSet = statement.executeQuery();
        String lastStarId = "";
        while(resultSet.next()){
            lastStarId = resultSet.getString("id");
        }
        String sub = lastStarId.substring(2);
        int x = Integer.parseInt(sub) + 1;
        String nextStarId = "nm" + x;
        statement.close();
        resultSet.close();
        return nextStarId;
    }
    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
