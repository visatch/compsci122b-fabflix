import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;


public class SAXParserStarMovieRelation extends DefaultHandler {
    List<StarMovieRelation> newStarMovieRelation;
    private String tempVal;
    //to maintain context
    private StarMovieRelation tempStarMovie;
    //private DataSource dataSource;
    public SAXParserStarMovieRelation() {
        newStarMovieRelation = new ArrayList<>();
    }

    public void runExample() {
        parseDocument();
        //printData();
    }

    private void parseDocument() {
        //get a factory
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            //get a new instance of parser
            SAXParser sp = spf.newSAXParser();
            //parse the file and also register this class for call backs
            sp.parse("casts124.xml", this);
        } catch (SAXException se) {
            se.printStackTrace();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    //Event Handlers
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //reset
        tempVal = "";
        if (qName.equalsIgnoreCase("m")) {
            //create a new instance of star
            tempStarMovie = new StarMovieRelation();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        tempVal = new String(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("m")) {
            //add it to the list
            newStarMovieRelation.add(tempStarMovie);
        } else if (qName.equalsIgnoreCase("f")) {
            tempStarMovie.setMovieId(tempVal);
        } else if (qName.equalsIgnoreCase("a")) {
            tempStarMovie.setStarName(tempVal);
        }
    }

    public void createFile(){
        File myObj = new File("starMovieRelation.sql");
        try {
            boolean newFile = myObj.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToFile(){
        try {
            FileWriter myWriter = new FileWriter("starMovieRelation.sql");
            myWriter.write("use moviedb;\n");
            //writing into the file
            for (StarMovieRelation newStarMovie: newStarMovieRelation) {
                String movieId = newStarMovie.getMovieId();
                String starId = checkStarExists(newStarMovie.getStarName());
                if(starId.equals("null")){
                    GlobalVariables.count_star_notFound ++;
                    continue;
                }
                myWriter.write("INSERT INTO stars_in_movies VALUES('" + starId + "','" + movieId + "');\n");
            }
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void insertStarMovieRelationIntoDB() {
        try {
            Connection dbCon = null;
            // Create a new connection to database
            String url = "jdbc:mysql://localhost:3306/moviedb";
            String user = "mytestuser";
            String password = "teamchocolate";
            dbCon = DriverManager.getConnection(url, user, password);

            // Read SQL file
            BufferedReader reader = new BufferedReader(new FileReader("starMovieRelation.sql"));

            String line;
            // Create PreparedStatement
            PreparedStatement preparedStatement = null;

            while ((line = reader.readLine()) != null) {
                preparedStatement = dbCon.prepareStatement(line);
                // Execute SQL statements
                try{
                    GlobalVariables.count_inserted_stars_in_movies ++;
                    preparedStatement.execute();
                }catch (SQLException e){
                    GlobalVariables.count_movie_notFound ++;
                }
            }
            // Close resources
            reader.close();
            assert preparedStatement != null;
            preparedStatement.close();
            dbCon.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String checkStarExists(String starName) {

        String result = "null";
        if(GlobalVariables.starHashMap.containsKey(starName)){
            result = GlobalVariables.starHashMap.get(starName);
        }
        return result;
    }

}
