import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DomMovieParser {
    List<Movie> movies = new ArrayList<>();
    Document dom;

    Integer inconsistencyCount = 0;
    Integer movieInsertedCount = 0;
    Integer duplicatedCount = 0;
    Integer movieIDEmptyCount = 0;
    Integer genreEmptyCount = 0;
    Integer genresInsertedCount = 0;
    Integer genresInMovieCount = 0;

    public void run() {

        parseXmlFile();

        parseDocument();

        try {
            addToDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\n\n");
        System.out.println("Inserted: " + movieInsertedCount + " movies.");
        System.out.println("Inserted:  " + genresInsertedCount + " genres.");
        System.out.println("Inserted:  " + genresInMovieCount + " genres_in_movies.");
        System.out.println(inconsistencyCount + " movies inconsistent (year contains characters...)");
        System.out.println(duplicatedCount + " movies duplicated");
    }

    private void parseXmlFile() {
        // get the factory
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        try {

            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            dom = documentBuilder.parse("mains243.xml");

        } catch (ParserConfigurationException | SAXException | IOException error) {
            error.printStackTrace();
        }
    }

    private void parseDocument() {
        // get the document root Element
        Element documentElement = dom.getDocumentElement();

        //Each set of document film
        NodeList filmList = documentElement.getElementsByTagName("film");

        try{
            FileWriter fwDuplicateMovie = new FileWriter(new File("DuplicateMovies.txt"));
            FileWriter fwInconsistency = new FileWriter(new File("InconsistentMovies.txt"));
            for (int i = 0; i < filmList.getLength(); i++) {
                Node filmNode = filmList.item(i);
                if (filmNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element filmElement = (Element) filmNode;

                    // Extracting details
                    String id = "";
                    if (filmElement.getElementsByTagName("fid").getLength() > 0){
                        id = filmElement.getElementsByTagName("fid").item(0).getTextContent().strip();
                    } else if (filmElement.getElementsByTagName("filmed").getLength() > 0) {
                        id = filmElement.getElementsByTagName("filmed").item(0).getTextContent().strip();
                    }

                    String title = filmElement.getElementsByTagName("t").item(0).getTextContent();
                    String year = filmElement.getElementsByTagName("year").item(0).getTextContent();

                    String director = "";

                    if (filmElement.getElementsByTagName("dir").getLength() > 0){
                        Element directorElement = (Element) filmElement.getElementsByTagName("dir").item(0);
                        if (directorElement.getElementsByTagName("dirn").getLength() > 0){
                            director = directorElement.getElementsByTagName("dirn").item(0).getTextContent();
                        }
                    }

                    String genre = "";
                    if (filmElement.getElementsByTagName("cat").getLength() > 0){
                        genre = filmElement.getElementsByTagName("cat").item(0).getTextContent().strip();
                    }

                    if (!year.chars().allMatch(Character::isDigit)){
                        fwInconsistency.write(id + '\n');
                        inconsistencyCount += 1;
                    }

                    Movie newMovie = new Movie(id,title,year,director,genre);
                    if (!movies.contains(newMovie)){
                        movies.add(newMovie);
                    }else{
                        fwDuplicateMovie.write(id + '\n');
                        duplicatedCount += 1;
                    }
                }
            }
            fwDuplicateMovie.close();;
            fwInconsistency.close();;

        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    public void addToDatabase() throws Exception {
        String loginUser = "mytestuser";
        String loginPasswd = "teamchocolate";
        String loginUrl = "jdbc:mysql://localhost:3306/moviedb";

        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        Connection connection = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
        String queryCallGenre = "{Call checkGenreInDB(?)}";
        String queryGetLastGenre = "{CALL getLastGenreId()}";
        String queryAddGenre = "{CALL add_genre(?,?)}";
        String queryCallToAddRTLS = "{Call add_genre_movie_relationship(?,?)}";
        String queryCallToAddMovie = "{Call add_movie(?,?,?,?)}";

        try{
            FileWriter fwMovieGenreEmpty = new FileWriter(new File("MovieGenreEmpty.txt"));
            FileWriter fwMovieIDEmpty= new FileWriter(new File("MovieIDEmpty.txt"));
            FileWriter fwGenreInserted = new FileWriter(new File("Genres.txt"));
            FileWriter fwDuplicateMovies = new FileWriter(new File("DuplicateMovies.txt"));

            for (int i = 0; i < movies.size(); i++) {
                if (movies.get(i).getGenre().isEmpty()) {
                    fwMovieGenreEmpty.write(movies.get(i).getId() + '\n');
                    genreEmptyCount += 1;
                    continue;
                }

                if (movies.get(i).getId().isEmpty()){
                    fwMovieIDEmpty.write(movies.get(i).getId() + '\n');
                    movieIDEmptyCount += 1;
                    continue;
                }
                CallableStatement statementGenre = connection.prepareCall(queryCallGenre);
                statementGenre.setString(1,movies.get(i).getGenre());
                ResultSet rs = statementGenre.executeQuery();

                String genreID = "";

                if (rs.next()){
                    genreID = rs.getString("id");
                } else {
                    CallableStatement statementGetLastGenreID = connection.prepareCall(queryGetLastGenre);
                    ResultSet rs1 = statementGetLastGenreID.executeQuery();
                    if (rs1.next()){
                        genreID = String.valueOf(Integer.parseInt(rs1.getString("id")) + 1);
                    }
                    rs1.close();

                    CallableStatement statementAddGenre = connection.prepareCall(queryAddGenre);
                    statementAddGenre.setInt(1,Integer.parseInt(genreID));
                    statementAddGenre.setString(2,movies.get(i).getGenre());
                    if (statementAddGenre.executeUpdate() > 0) {
                        fwGenreInserted.write(movies.get(i).getGenre() + '\n');
                        genresInsertedCount += 1;
                    }
                }

                CallableStatement statementAddMovie = connection.prepareCall(queryCallToAddMovie);
                statementAddMovie.setString(1,movies.get(i).getId());
                statementAddMovie.setString(2,movies.get(i).getTitle());

                if (movies.get(i).getYear().chars().allMatch(Character::isDigit)){
                    statementAddMovie.setString(3,movies.get(i).getYear());
                }else {
                    statementAddMovie.setNull(3,Types.INTEGER);
                }

                statementAddMovie.setString(4,movies.get(i).getDirector());
                try{
                    statementAddMovie.execute();
                    movieInsertedCount += 1;

                    CallableStatement statementAddRTLS = connection.prepareCall(queryCallToAddRTLS);
                    statementAddRTLS.setString(1, genreID);
                    statementAddRTLS.setString(2,movies.get(i).getId());
                    statementAddRTLS.executeQuery();
                    fwGenreInserted.write(genreID);
                    genresInMovieCount += 1;

                }catch (SQLException e){
                    fwDuplicateMovies.write(movies.get(i).getId());
                    duplicatedCount += 1;
                }

                rs.close();
            }

            fwMovieIDEmpty.close();
            fwMovieGenreEmpty.close();
            fwGenreInserted.close();
            fwDuplicateMovies.close();

        }catch (IOException e){
            System.out.println("ERROR WRITING DATA INTO THE FILE!!!");
        }finally {
            connection.close();
        }
        connection.close();
    }

    public static void main(String[] args) {
        DomMovieParser domMovieParser = new DomMovieParser();

        domMovieParser.run();
    }
}
