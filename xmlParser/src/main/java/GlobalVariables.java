import java.util.HashMap;
import java.util.List;

public class GlobalVariables {
    public static List<Star> newStars;
    public static HashMap<String, String> starHashMap = new HashMap<>();
    public static String tempVal;
    public static Star tempStar;

    public static int count_star_notFound = 0;
    public static int count_movie_notFound = 0;

    public static int count_inserted_star = 0;
    public static int count_inserted_stars_in_movies = 0;

    public static int count_star_duplicate = 0;
}
