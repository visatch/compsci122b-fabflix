import java.util.Objects;

public class Movie {
    private final String id;

    private final String title;
    private final String year;
    private final String director;

//    private String[] genres;
    private final String genre;

    public Movie(String id, String title, String year, String director, String genre) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.director = director;
//        this.genres = genres;
        this.genre = genre;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

//    public String getGenres() {
//        return Arrays.toString(genres);
//    }

    public String getGenre(){
        return genre;
    }

    @Override
    public String toString() {
        return "Movie ID: " + getId() + " , Title: " + getTitle() + " , Year: " + getYear() + " , Genres: " + getGenre() + " , Director: " + getDirector() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(id, movie.id) && Objects.equals(title, movie.title) && Objects.equals(year, movie.year) && Objects.equals(director, movie.director) && Objects.equals(genre, movie.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, year, director, genre);
    }
}
